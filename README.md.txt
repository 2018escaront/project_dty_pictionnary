Projet DTY pictionnary
réalisé par Tanguy Escaron


description fonctionnelle:
Projet DTY pictionnary est une application web qui permet de jouer au pictionnary à plusieurs en local

Prérequis techniques : 
Node version 10.14.1
MySQL version 5.7.21

description succinte:
Ce projet est composé d'un backend avec un server express, d'un front en ReactJS 6.4.1, d'une BDD MySQL 5.7.20, authentification par JWT géré "à la main", les requettes HTTP faites au server sont réalisées à l'aide d'axios , les mots de passe sont encodés en utilisant bcrypt, le lien avec la base de donnée se fait avec sequelize 5.5.0

procédure d'installation détaillée:
Lancer les commandes suivantes :  
/ Pour créer la BDD et charger le schéma  dans le dossier pictionnary_backend: "sequelize db:create", "sequelize db:migrate", "sequelize db:seed:all"
/ Pour lancer le  back dans le dossier pictionnary_backend :  "npm install", "node server/server.js", vous devriez lire "server a l'ecoute"
/ Pour lancer le front dans le dossier pictionnary_frontend: "npm install", "npm start", l'application devrait se lancer sur votre navigateur web à l'adresse "https://localhost:3000"


comment précharger le user "root" : 
La commande "sequelize db:seed:all" permet de charger un premier mot et le user root.
ce user est admin, il possède pour adresse mail "root@root.fr" et comme mdp "root123", il est capable de voir l'historique des parties crées, il peut les supprimer de la base de données, il peut également voir tous les mots à faire deviner et en ajouter/supprimer.


description du jeu de données: 
La base de données est composée de 2 tables,
une table Users qui possède les attributs (id, email, username, password, isAdmin, playingPartieId, nbPoints, nbLocalPoints (points au sein de la partie en cours), localWinner (gagnant au sein de la partie en cours), nbPartiesWon, nbPartiesPlayed, nbPartiesHosted)
une table Parties qui possède les attributs (id, creatorId (id d'un User qui a créé la partie), partieName, isEnded, nbRounds (nombre de manches restantes), players (nombre de joueurs connectés à la partie), places (nombre maximum de joueurs))
une table Mots qui possède les attributs (id, word)

URL du projet:
le code source est accessible à "https://gitlab-student.centralesupelec.fr/2018escaront/project_dty_pictionnary"
pour l'instant on joue sur "https://localhost:3000" ...

fonctionnalités:
On peut creer un utilisateur, 
se login avec un utilisateur enregistré, visualiser/rejoindre/créer une partie, visualiser mon profil
Jouer à un jeu et tenter de deviner le mot secret, lorsqu'on est créateur d'une partie, on peut dessiner pour faire deviner un mot à d'autres joueurs
Se login en tant qu'administrateur pour visualiser/supprimer l'historique des parties, visualiser/ajouter/supprimer un mot à faire deviner
