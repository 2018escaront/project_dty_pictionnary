const initialState = { 
    userId: null,
    token: "",
    isAdmin: false,
    username: ""
}

function manageUser(state = initialState, action) {
    let nextState;
    switch (action.type) {
        case 'CREATE_USER':
            nextState = {
                ...state,
                token: action.value.token,
                username: action.value.username,
                userId: action.value.userId,
                isAdmin: action.value.isAdmin
            }
            return nextState || state
        case 'DELETE_USER':
            nextState = {
                ...state,
                token: "",
                username: "",
                userId: null,
                isAdmin: false
            }
            return nextState || state
        default:
            return state
    }
}

export default manageUser