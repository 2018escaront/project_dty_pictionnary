import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import manageUser from './Reducers/userReducer'


const persistConfig = {
  key: 'root',
  storage,
}
 
const persistedReducer = persistReducer(persistConfig, manageUser)
const myPersistStore = createStore(persistedReducer)
const persistor = persistStore(myPersistStore)
const store = createStore(manageUser)
export default { myPersistStore, persistor, store }