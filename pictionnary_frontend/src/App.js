import React from 'react';
import Routes from './Routes';
import { Provider } from 'react-redux';
import storePersistor from './Store/configureStore';
import { PersistGate } from 'redux-persist/integration/react'

import '../node_modules/uikit/dist/css/uikit.css';
import '../node_modules/uikit/dist/css/uikit-core-rtl.css';
import '../node_modules/uikit/dist/css/uikit-core.css';
import '../node_modules/uikit/dist/css/uikit-rtl.css';
import '../node_modules/uikit/dist/js/uikit.js';
import '../node_modules/uikit/dist/js/uikit-icons.js';
import '../node_modules/uikit/dist/js/uikit-core.js';

const { store, myPersistStore, persistor } = storePersistor;




function App() {
    
    return (
        <Provider store={ store }>
            {/* <PersistGate loading={ null } persistor={ persistor }> */}
                <div >
                    <nav className="uk-navbar uk-navbar-container">
                        <div className="uk-navbar-left">
                            <ul className="uk-navbar-nav">
                                <h4>Pictionary online</h4>
                            </ul>
                        </div>
                    </nav>
                    <Routes/>
                </div>
            {/* </PersistGate> */}
        </Provider>
    );
}

export default App;
