import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';

import Room from './Views/Room'
import HomePage from './Views/HomePage';
import UserHomePage from './Views/UserHomePage';
import AdminHomePage from './Views/AdminHomePage';
import Register from './Views/Register';
import CreatePartie from './Components/CreatePartie';

const Routes = (props) => {
    return (
    <BrowserRouter  >
        <Switch>
            <Route exact path="/">
                <HomePage/>
            </Route> 
            <Route exact path="/room/:id" component={Room}/>
            <Route exact sensitive path="/user/" component={UserHomePage}/>
            <Route exact sensitive path="/admin/" component={AdminHomePage}/>
            <Route exact path="/register">
                <Register/>
            </Route>
            <Route exact path="/createPartie">
                <CreatePartie/>
            </Route>
        </Switch>
    </BrowserRouter>
);
}

export default Routes;