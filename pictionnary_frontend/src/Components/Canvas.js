import React from 'react';



class Canvas extends React.Component { 
    constructor (props) {
        super (props);
        this.canvas = null;
        this.context = null;
        this.state = { ctx: null }
        this.mounted = false;
        this.mouse = {
            click: false,
            move: false,
            pos: {x:0, y:0},
            pos_prev: false
        };
        this.width = 4;
        this.color = "#000000";
        this.lines = [];
        this.mainLoop = this.mainLoop.bind(this);
        this.draw = this.draw.bind(this);
        this.clearLast = this.clearLast.bind(this);
        
    }
    componentDidMount () {
        this.canvas = this.refs.canvas;
        this.setState({ ctx: this.canvas.getContext('2d') })
        
        this.canvas.width = window.innerWidth*1/2;
        this.canvas.height = window.innerHeight;
        this.canvas.style.background = "grey"

        this.rect = this.canvas.getBoundingClientRect();
        
        this.props.socket.on('join', function (data) {
            // Server tells if you are "drawer" or "devineur"
            if (data.type === "drawer") {
                this.type = data.type;
                this.canvas.onmousedown = function(e){
                    this.mouse.click = true;
                }.bind(this);
                this.canvas.onmouseup = function(e){
                    this.mouse.click = false;
                    this.props.socket.emit('draw', { roomId: this.props.roomId, lines: this.lines, width: this.width, color: this.color});
                    this.lines = [];
                }.bind(this);
                this.canvas.onmousemove = function(e) {
                    //position relative au canvas
                    this.mouse.pos.x = (e.clientX-(this.canvas.offsetLeft-window.scrollX))/this.canvas.width;
                    this.mouse.pos.y = (e.clientY-(this.canvas.offsetTop-window.scrollY))/this.canvas.height;
                    this.mouse.move = true;
                }.bind(this);
                this.mainLoop();
            }
        }.bind(this));
        this.props.socket.on('draw', function (data) {
            console.log(data)
            if (this.mounted) {
                let { lines, width, color } = data;
                for (let line of lines){
                    this.draw(line, width, color);
                }
            }
        }.bind(this));
        this.props.socket.on('clear', function (data) {
            if (this.mounted) {
                this.state.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            }
        }.bind(this));
        this.mounted = true;
    }
    draw(line, width, color) {
        this.state.ctx.strokeStyle = color;
        this.state.ctx.lineWidth = width;
        this.state.ctx.beginPath();
        this.state.ctx.moveTo(line[0].x*this.canvas.width, line[0].y*this.canvas.height);
        this.state.ctx.lineTo(line[1].x*this.canvas.width, line[1].y*this.canvas.height);
        this.state.ctx.stroke();
    }
    clearLast() {
        this.props.socket.emit('clear last', { roomId: this.props.roomId });
    }
    mainLoop() {
		if (this.mouse.click && this.mouse.move && this.mouse.pos_prev) {
            const line = [  {x: this.mouse.pos.x, y: this.mouse.pos.y},  {x: this.mouse.pos_prev.x, y: this.mouse.pos_prev.y} ];
            this.lines = [...this.lines, line];
            this.mouse.move = false;
            this.draw(line, this.width, this.color);
        }
		this.mouse.pos_prev = {x: this.mouse.pos.x, y: this.mouse.pos.y};
        setTimeout(this.mainLoop, 25);
	}

    render () {
        return (
            <div>
                <canvas ref="canvas"/>
                {this.type === "drawer" &&
                <div>
                    <div className="uk-flex">
                        <button className="uk-button uk-button-default" onClick={this.clearLast}>Back one step</button>
                    </div>
                    <div className="uk-flex">
                        <button className="uk-button uk-button-default" onClick={() => this.width = 8}>Set Fat Width</button>
                        <button className="uk-button uk-button-default" onClick={() => this.width = 2}>Set Small Width</button>
                        <button className="uk-button uk-button-default" onClick={() => this.width = 4}>Set Normal Width</button>
                    </div>
                    <br/>
                    <div className="uk-flex">
                        <button className="uk-button uk-button-default" onClick={() => this.color = "#FF0000"}>Set Color Red</button>
                        <button className="uk-button uk-button-default" onClick={() => this.color = "#00FF00"}>Set Color Green</button>
                        <button className="uk-button uk-button-default" onClick={() => this.color = "#0000FF"}>Set Color Blue</button>
                        <button className="uk-button uk-button-default" onClick={() => this.color = "#000000"}>Set Color Black</button>
                    </div>
                </div>
                }
            </div>
        );
    }
}


export default Canvas;

