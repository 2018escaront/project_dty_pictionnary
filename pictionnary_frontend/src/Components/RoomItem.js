import React from 'react'

class RoomItem extends React.Component {
    constructor (props) {
        super (props);
        // binding de mes fonctions de callback
        this._onSupprime = this._onSupprime.bind(this);
        this._onJoin = this._onJoin.bind(this);
    }
    _onJoin () {
        // Rappel de la fonction parente pour rejoindre cette partie
        this.props.onJoin(this.props.id);
    }
    _onSupprime () {
        // Rappel de la fonction parente pour supprimer cette partie
        this.props.onSupprime(this.props.id);
    }
    render () {
        const { type, partieName, players, places, isEnded, nbRounds, creatorId, createdAt } = this.props;
        return (
            <div className="uk-margin-small-top uk-margin-small-left uk-margin-small-right uk-margin-small-bottom">
                <h3 className="uk-text-center">{partieName}</h3>
                {players}/{places} players<br/>
                {nbRounds} rounds<br/>
                {type === "admin" &&
                    /* Acces à la suppression pour l'admin */
                    <div>
                        Creator id : {creatorId}<br/>
                        Created at : {createdAt}<br/>
                        {isEnded && <p>Partie Terminee</p>}
                        <button className="uk-button uk-button-default" onClick={this._onSupprime} >Supress </button>
                    </div>
                }
                {type === "player" &&
                    /* Acces au join pour un user dans le cas où il reste de la place*/
                    <div>
                        {players < places && <button className="uk-button uk-button-default" onClick={this._onJoin} >Join</button>}
                    </div>
                }
            </div>
        )
    }
}
  

export default RoomItem;