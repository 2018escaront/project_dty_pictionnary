import React from 'react';

// Component de message d'erreur
const ErrorMessage = (props) => {
    return (
        <div>
            <h3>Erreur numéro {props.status}</h3>
            <p>{props.message}</p>
        </div>
    )
}

export default ErrorMessage

