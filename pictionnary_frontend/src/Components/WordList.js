import React from 'react';
import Word from './Word';


class WordList extends React.Component {
    render () {
        return (
            <div className="uk-grid uk-margin-top">
                {this.props.words.map(({id, word}) => {
                    return <div  key={id} className="uk-width-1-5 uk-margin-small">
                        <Word word={word} id={id} onSupprime={this.props.onSupprime}/>
                    </div>
                })}
            </div>
        )
    }
}

export default WordList;