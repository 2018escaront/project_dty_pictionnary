import React from 'react';

class Word extends React.Component { 
    constructor (props) {
        super (props);

        this._supprimeWord = this._supprimeWord.bind(this);
    }
    _supprimeWord () {
        // Rappel de la fonction parente pour supprimer ce mot
        this.props.onSupprime(this.props.word)
    }
    render() {
        return (
        <div className="uk-card uk-card-default uk-card-body">
            <h5 className="uk-card-title">{this.props.word}</h5>
            <button className="uk-button uk-button-default" onClick={this._supprimeWord}>Supprimer</button>
        </div>
        )
    }
}


export default Word;