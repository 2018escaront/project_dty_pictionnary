import React from 'react';
import RoomItem from './RoomItem';

// Implémente une liste de RoomItem
class RoomList extends React.Component {
    render () {
        return (
            <div className="uk-grid uk-margin-top">
            {this.props.rooms.map((room, i) => {
                return ( 
                    <div key={i} className="uk-width-1-6 uk-card uk-card-default uk-margin-medium-top uk-margin-medium-left uk-margin-medium-right uk-margin-medium-bottom">
                        <RoomItem
                            id={room.id}
                            nbRounds={room.nbRounds}
                            players={room.players}
                            places={room.places}
                            partieName={room.partieName}
                            creatorId={room.creatorId}
                            createdAt={room.createdAt}
                            isEnded={room.isEnded}
                            onJoin={this.props.onJoin}
                            onSupprime={this.props.onSupprime}
                            type={this.props.type}
                        />
                    </div>
                )
            })}
            </div>
        )
    }
    
}

export default RoomList;