import React from 'react';

import { getUser, getPartiesCreated } from '../utils/Api';
import ErrorMessage from './ErrorMessage'
import RoomList from './RoomList';

const NO_ERROR = {
    error: false,
    status: 200,
    message: ""
};

class CreatePartie extends React.Component {
    
    constructor (props) {
        super (props);
        this.state = {
            error: NO_ERROR,
            user: { id: null, email: null, username: null, nbPartiesWon: null, nbPartiesPlayed: null, nbPartiesHosted: null, nbPoints: null },
            rooms: []
        }

        this.handleError = this.handleError.bind(this);
    }
    async componentDidMount () {
        const user = await getUser(this.props.userId, this.props.token, this.handleError);
        const rooms = await getPartiesCreated(this.props.userId, this.props.token, this.handleError);
        this.setState({
            user: user,
            rooms: rooms
        })

    }
    handleError(error){
        // Gestion des erreurs
        console.log(error)
        let status = 500;
        let message = "error occured, please refresh page";
        if (error && error.response) {
            status = error.response.status;
            message = "error : " + error.response.data.error + "; message : " + error.response.data.message;
        } else if (error) {
            message = error;
        }
        
        this.setState({
            error: {
                error: true,
                status: status,
                message: message
            }
        });
    }
    render() {
        const { id, email, username, nbPartiesWon, nbPartiesPlayed, nbPartiesHosted, nbPoints } = this.state.user;
        const { error } = this.state;
        // Error handling
        if (error.error && error.status === 500) {
            return <ErrorMessage status={error.status} message={error.message} />
        }
        return (
        
            <div className=" uk-container uk-flex">
                <div className="uk-card-default uk-card-body uk-width-1-4">
                    <p>Id : {id}</p>
                    <p>Email : {email}</p>
                    <p>Username : {username}</p>
                    <p>Number of games Won : {nbPartiesWon}</p>
                    <p>Number of games played : {nbPartiesPlayed}</p>
                    <p>Number of points : {nbPoints}</p>
                    <p>Number of games hosted : {nbPartiesHosted}</p>
                </div>
                <div>
                    <h5 className="uk-margin-large-left uk-text-lead">Games created :</h5>
                    <RoomList 
                        rooms={this.state.rooms} 
                        type="profile"
                        className="uk-width-1-2"
                    />
                </div>
            </div>
        )
    }
}



export default CreatePartie;