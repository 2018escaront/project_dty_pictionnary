import React from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom'

import { createPartie } from '../utils/Api';
import ErrorMessage from './ErrorMessage'

const NO_ERROR = {
    error: false,
    status: 200,
    message: ""
};
const NUMBER_REGEX = /^([0-9]|[0-9][0-9])$/;
const TEXT_REGEX = /^([1-zA-Z0-1@.\s]{1,255})$/;
const PARTIE_NAME_LIMIT = 4;

class CreatePartie extends React.Component {
    constructor (props) {
        super (props);
        
        this.state = {
            partieCreated: false,
            roomId: null,
            error: NO_ERROR,
            places: 0,
            rounds: 0,
            partieName: "",

            placesClassName: "uk-input uk-text-center",
            partieNameClassName: "uk-input uk-text-center",
            roundsClassName: "uk-input uk-text-center"
        };

        //binding functions
        this._partieNameChange = this._partieNameChange.bind(this);
        this._placesChange = this._placesChange.bind(this);
        this._createPartie = this._createPartie.bind(this);
        this._roundsChange = this._roundsChange.bind(this);
        this.handleError = this.handleError.bind(this);
    }
    handleError(error){
        // Gestion des erreurs
        console.log(error)
        let status = 500;
        let message = "error occured, please refresh page";
        if (error && error.response) {
            status = error.response.status;
            message = "error : " + error.response.data.error + "; message : " + error.response.data.message;
        } else if (error) {
            message = error;
        }
        
        this.setState({
            error: {
                error: true,
                status: status,
                message: message
            }
        });
    }
    _partieNameChange(event) {
        const partieName = event.target.value;
        let partieNameClassName = "uk-input uk-text-center";
        if (!partieName || partieName.length <= PARTIE_NAME_LIMIT || !TEXT_REGEX.test(partieName)) {
            partieNameClassName += " uk-form-danger";
        };
        this.setState({
            partieNameClassName: partieNameClassName,
            partieName: partieName
        });
    }
    _placesChange(event) {    
        const places = event.target.value;
        let placesClassName = "uk-input uk-text-center";
        if (!places || places < 2 || !NUMBER_REGEX.test(places)) {
            placesClassName += " uk-form-danger";
        };
        this.setState({
            placesClassName: placesClassName,
            places: places
        });
    }
    _roundsChange(event) {    
        const rounds = event.target.value;
        let roundsClassName = "uk-input uk-text-center";
        if (!rounds || !NUMBER_REGEX.test(rounds)) {
            roundsClassName += " uk-form-danger";
        };
        this.setState({
            roundsClassName: roundsClassName,
            rounds: rounds
        });
    }
    async _createPartie(event) {
        event.preventDefault();
        // Check if parameters are valid
        const { partieName, places, rounds } = this.state;
        if (!partieName || partieName.length <= PARTIE_NAME_LIMIT) {
            this.handleError("Nom de partie trop court");
        } else if (places < 2) {
            this.handleError("Il faut au moins 2 places");
        } else if (!NUMBER_REGEX.test(places) || !TEXT_REGEX.test(partieName) || !NUMBER_REGEX.test(rounds)) {
            this.handleError("Mauvais format de données");
        } else {
            // Create partie
            const { partieCreated, roomId } = await createPartie(partieName, places, rounds, this.props.token, this.handleError);
            this.setState({
                partieCreated: partieCreated,
                roomId: roomId
            });
        }
        
    }
    render() {
        const { error, partieCreated, roomId, partieNameClassName, placesClassName, roundsClassName } = this.state;
        // Error handling
        if (error.error && error.status === 500) {
            return <ErrorMessage status={error.status} message={error.message} />
        } else if (partieCreated) {
            // Redirecting to game when a game was created
            const route = "/room/" + roomId;
            return  <Redirect from="/createPartie" to={route} />
        }
        return (
        
            <div className=" uk-container uk-flex uk-flex-center">
                <div className="uk-width-1-3">
                    <h3 className="uk-heading-small uk-text-center">New Game</h3>
                    <form onSubmit={this._createPartie} className="uk-panel uk-panel-box  uk-form-stacked uk-margin-large">
                        <div className="uk-margin-small">
                            <label className="uk-form-label uk-text-center" >Game Name</label>
                            <input 
                                className = {partieNameClassName}
                                type = "text"
                                placeholder = "My game"
                                onChange = {this._partieNameChange}
                            />
                        </div>
                        <div className="uk-margin-small">
                            <label className="uk-form-label uk-text-center" >Max Participants</label>
                            <input 
                                className = {placesClassName}
                                type = "number"
                                placeholder = {3}
                                onChange = {this._placesChange}
                            />
                        </div>
                        <div className="uk-margin-small">
                            <label className="uk-form-label uk-text-center" >Number of rounds</label>
                            <input 
                                className = {roundsClassName}
                                type = "number"
                                placeholder = {3}
                                onChange = {this._roundsChange}
                            />
                        </div>
                        <div className="uk-margin-medium">
                            {error.error && <p className="uk-text-center uk-text-danger">{error.message}</p>}
                            <input 
                                className = "uk-input uk-button uk-button-default"
                                type="submit" 
                                value="Create New Game" 
                            />
                            <div className=" uk-container uk-flex uk-flex-center">
                                <Link to={`/user`}>
                                    Cancel
                                </Link>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
          token: state.token
      }
  }


export default connect(mapStateToProps)(CreatePartie);