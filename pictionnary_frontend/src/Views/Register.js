import React from 'react';
import { Link, Redirect } from 'react-router-dom';

import ErrorMessage from '../Components/ErrorMessage';
import { register } from '../utils/Api';

const NO_ERROR = {
    error: false,
    status: 200,
    message: ""
};
const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX = /^(?=.*\d).{4,8}$/;

class Register extends React.Component {
    constructor (props) {
        super (props);

        this.state = {
            userCreated: false,
            error: NO_ERROR,

            email: "",
            password: "",
            confirmPassword: "",
            username: "",

            emailClassName: "uk-input uk-text-center",
            passwordClassName: "uk-input uk-text-center",
            confirmPasswordClassName: "uk-input uk-text-center",
            usernameClassName: "uk-input uk-text-center",

        }

        // binding de mes fonctions
        this.handleError = this.handleError.bind(this);
        this._passwordChange = this._passwordChange.bind(this);
        this._emailChange = this._emailChange.bind(this);
        this._usernameChange = this._usernameChange.bind(this);
        this._confirmPasswordChange = this._confirmPasswordChange.bind(this);
        this.handleRegister = this.handleRegister.bind(this);
    } 
    _emailChange(event) {
        const email = event.target.value;
        let emailClassName = "uk-input uk-text-center";
        if (email === "" || !EMAIL_REGEX.test(email)) {
            emailClassName += " uk-form-danger";
        }
        this.setState({
            emailClassName: emailClassName,
            email: email
        });
    }
    _usernameChange(event) {
        const username = event.target.value;
        let usernameClassName = "uk-input uk-text-center";
        if (username === "" || username.length >= 13 || username.length <= 4) {
            usernameClassName += " uk-form-danger";
        }
        this.setState({
            usernameClassName: usernameClassName,
            username: username
        });
    }
    _passwordChange(event) {
        const password = event.target.value;
        let passwordClassName = "uk-input uk-text-center";
        if (password === "" || !PASSWORD_REGEX.test(password)) {
            passwordClassName += " uk-form-danger";
        }
        this.setState({
            passwordClassName: passwordClassName,
            password: password
        });
    }
    _confirmPasswordChange(event) {
        const confirmPassword = event.target.value;
        let confirmPasswordClassName = "uk-input uk-text-center";
        if (confirmPassword === "" || this.state.password !== confirmPassword) {
            confirmPasswordClassName += " uk-form-danger";
        }
        this.setState({
            confirmPasswordClassName: confirmPasswordClassName,
            confirmPassword: confirmPassword
        });
    }
    handleError (error) {
        // Gestion des erreurs
        console.log(error)
        let status = 500;
        let message = "error occured, please refresh page";
        if (error && error.response) {
            status = error.response.status;
            message = "error : " + error.response.data.error + "; message : " + error.response.data.message;
        } else if (error) {
            message = error;
        }
        
        this.setState({
            error: {
                error: true,
                status: status,
                message: message
            }
        });
    }

    // Création d'un nouveau compte
    async handleRegister(event) {
        const { username, password, email, confirmPassword } = this.state;

        event.preventDefault();
        // Verifications
        if (email === "" || username === "" || password === "" || confirmPassword === "") {
            this.handleError("Missing parameters");
        } else if (username.length >= 13 || username.length <= 4) {
            this.handleError("Username should be between 5 and 12 characters");
        } else if (!EMAIL_REGEX.test(email)) {
            this.handleError("Wrong email format");
        } else if (!PASSWORD_REGEX.test(password)) {
            this.handleError("Password must be between 4 and 8 chars with at least a number");
        } else if (password !== confirmPassword) {
            this.handleError("Confirmation password doesn't match password");
        } else {
            // Création
            const userCreated = await register(email, username, password, confirmPassword, this.handleError);
            this.setState({
                userCreated: userCreated
            });
        }
    }

    render() {
        const { error, userCreated, emailClassName, passwordClassName, usernameClassName, confirmPasswordClassName } = this.state;

        // Gestion des erreurs
        if (error.error && error.status === 500) {
            return <ErrorMessage status={error.status} message={error.message} />
        } else if (userCreated) {
            //popup user created
            return <Redirect from="/register" to="/" />
        }
        return (
            <div className=" uk-container uk-flex uk-flex-center">
                <div className="uk-width-1-3">
                    <h3 className="uk-heading-small uk-text-center">Register</h3>
                    <form onSubmit={this.handleRegister} className="uk-panel uk-panel-box  uk-form-stacked uk-margin-large">
                        <div className="uk-margin-small">
                            <label className="uk-form-label uk-text-center" >Email</label>
                            <input 
                                className = {emailClassName}
                                type = "text"
                                placeholder = "example@example.fr"
                                onChange = {this._emailChange}
                            />
                        </div>
                        <div className="uk-margin-small">
                            <label className="uk-form-label uk-text-center" >Username</label>
                            <input 
                                className = {usernameClassName}
                                type = "text"
                                placeholder = "username"
                                onChange = {this._usernameChange}
                            />
                        </div>
                        <div className="uk-margin-small">
                            <label className="uk-form-label uk-text-center" >Password</label>
                            <input 
                                className = {passwordClassName}
                                type = "password"
                                placeholder = "password"
                                onChange = {this._passwordChange}
                            />
                        </div>
                        <div className="uk-margin-small">
                            <label className="uk-form-label uk-text-center" >Confirm password</label>
                            <input 
                                className = {confirmPasswordClassName}
                                type = "password"
                                placeholder = "confirm password"
                                onChange = {this._confirmPasswordChange}
                            />
                        </div>
                            {error.error && <p className="uk-text-center uk-text-danger">{error.message}</p>}
                            <input 
                                className = "uk-input uk-button uk-button-default"
                                type="submit" 
                                value="Create New User" 
                            />
                    </form>
                    <Link to={`/`}>
                        Cancel
                    </Link>
                </div>
            </div>
        )
    }
}

export default Register;