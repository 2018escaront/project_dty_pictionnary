import React from 'react'
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom';

import RoomList from '../Components/RoomList';
import WordList from '../Components/WordList';
import ErrorMessage from '../Components/ErrorMessage'

import { getAllRooms, getWords, addWord, deleteWord, deleteRoom } from '../utils/Api';

const NO_ERROR = {
    error: false,
    status: 200,
    message: ""
};
const NUMBER_REGEX = /^([0-9]|[0-9][0-9])$/;
const WORD_REGEX = /^([a-z]{1,})$|^$/

class HomePage extends React.Component {
    constructor (props) {
        super (props);

        this.state = {
            status: "rooms",
            words: [],
            rooms: [],
            error: NO_ERROR,
            word: "",
            wordClassName: "uk-input",
            disconnected: false
        }
        this.nbrEndedRooms = 0;
        this.nbrRooms = 0;
        this.nbrWords = 0;

        //input
        this.word = "";
        this.roomsClassName = "uk-active";
        this.wordsClassName = "";

        // binding functions
        this._displayRooms = this._displayRooms.bind(this);
        this._displayWords = this._displayWords.bind(this);
        this._showRooms = this._showRooms.bind(this);
        this._showWords = this._showWords.bind(this);
        this._addWord = this._addWord.bind(this);
        this._refresh = this._refresh.bind(this);
        this._disconnect = this._disconnect.bind(this);
        this._handleWordChange = this._handleWordChange.bind(this);
        this.onSupprimeWord = this.onSupprimeWord.bind(this);
        this.onSupprimeRoom = this.onSupprimeRoom.bind(this);
        this.handleError = this.handleError.bind(this);
    }

    
    handleError(error){
        // Gestion des erreurs
        console.log(error)
        let status = 500;
        let message = "error occured, please refresh page";
        if (error && error.response) {
            status = error.response.status;
            message = "error : " + error.response.data.error + "; message : " + error.response.data.message;
        } else if (error) {
            message = error;
        }
        
        this.setState({
            error: {
                error: true,
                status: status,
                message: message
            }
        });
    }
    _handleWordChange(event) {
        const lowercase_word = event.target.value.toLowerCase();
        let wordClassName = "uk-input";
        if (!lowercase_word || !WORD_REGEX.test(lowercase_word)) {
            wordClassName += " uk-form-danger";
        };
        if (!WORD_REGEX.test(lowercase_word)) {
            this.handleError("word format incorrect");
        }
        this.setState({
            wordClassName: wordClassName,
            word: lowercase_word
        });
    }
    componentDidMount() {
        // Initialisation de l'erreur, des words et des rooms
        this._refresh();
    }
    // Appelée depuis un RoomItem, permet de supprimer une room
    async onSupprimeRoom(roomId) {
        // Verification du parametre
        if (!NUMBER_REGEX.test(roomId)) {
            this.handleError("Room Id incorrect");
        } else {
            const rooms = await deleteRoom(roomId, this.props.token, this.handleError);
            if (rooms) {
                this.nbrEndedRooms = rooms.filter(room => room.isEnded).length;
                this.nbrRooms = rooms.length;
                this.setState({
                    rooms: rooms
                });
            }
        }
    }
    // Appelée depuis un Word, permet de supprimer un word
    async onSupprimeWord(word) {
        // Verification du parametre
        const lowercase_word = word.toLowerCase();
        if (!WORD_REGEX.test(lowercase_word)) {
            this.handleError("word format incorrect");
        } else {
            // Suppression du mot
            const words = await deleteWord(lowercase_word, this.props.token, this.handleError);
            this.nbrWords = words.length;
            this.setState({
                words: words
            });
        }
    }
    // Ajout d'un word
    async _addWord(e) {
        e.preventDefault();

        // Verification du parametre
        const lowercase_word = this.state.word.toLowerCase();
        if (!lowercase_word || !WORD_REGEX.test(lowercase_word)) {
            this.handleError("word format incorrect");
        } else {
            // Ajout du mot
            const words = await addWord(lowercase_word, this.props.token, this.handleError);
            this.nbrWords = words.length;
            this.setState({
                words: words
            });
        }
    }
    // Basculer sur l'onglet rooms
    _showRooms(event) {
        this.roomsClassName = "uk-active";
        this.wordsClassName = "";
        this.setState({
            status: "rooms"
        });
    }
    // Basculer sur l'onglet words
    _showWords(event) {
        this.wordsClassName = "uk-active";
        this.roomsClassName = "";
        this.setState({
            status: "words"
        });
    }
    // Mise à jour de l'erreur, des rooms et des words
    async _refresh () {
        this.setState({
            error: NO_ERROR
        })
        const rooms = await getAllRooms(this.props.token, this.handleError);
        const words = await getWords(this.props.token, this.handleError);
        this.nbrEndedRooms = rooms.filter(room => room.isEnded).length;
        this.nbrRooms = rooms.length;
        this.nbrWords = words.length;
        this.setState({
            rooms: rooms,
            words: words,
        });
    }
    _disconnect () {
        // Deconnexion de l'admin
        const action = { type: "DELETE_USER" };
        this.props.dispatch(action);
        this.setState({
            disconnected: true
        })
    }

    _displayRooms() {
        // Affichage de l'onglet rooms
        if (this.state.status === "rooms") {
            return (
                <div className="uk-margin-left">
                    <p>{this.nbrRooms} total rooms, and {this.nbrEndedRooms} ended ones</p>
                    <RoomList 
                        rooms={this.state.rooms} 
                        onSupprime={this.onSupprimeRoom}
                        type="admin"
                    />
                </div>
            )
        }
    }
    _displayWords() {
        // Affichage de l'onglet words
        const { word, wordClassName, words, status, error } = this.state;
        if (status === "words") {
            return (
                <div className="uk-margin-left">
                    
                    <p>{this.nbrWords} total words</p>
                    <WordList 
                        words={words} 
                        onSupprime={this.onSupprimeWord}
                    />
                    <hr/>
                    {/* Form d'ajout d'un mot */}
                    <div className="uk-card uk-card-default uk-width-1-5">
                    <form onSubmit={this._addWord} className=" uk-form-stacked uk-margin uk-card-body">
                        <label className="uk-form-label uk-card-title" >New word</label>
                        <input
                            value={word}
                            className = {wordClassName}
                            type = "text"
                            placeholder = "word"
                            onChange = {this._handleWordChange}
                        />
                        <div className="uk-margin-top">
                            {error.error && <p className="uk-text-center uk-text-danger">{error.message}</p>}
                            <input 
                                className = "uk-input uk-button uk-button-default"
                                type="submit" 
                                value="Add word" 
                            />
                        </div>
                    </form>
                    </div>
                </div>
            )
        }
    }

    render() {
        const { error, disconnected } = this.state;
        const { token } = this.props;
        
        if (error.error && error.status === 500) {
            return <ErrorMessage status={error.status} message={error.message}/>
        } else if (disconnected || token === "") {
            return <Redirect from="/admin" to="/"/>
        }
        return (
        <div >
            <div>
                <nav className="uk-navbar uk-navbar-container">
                    <div className="uk-navbar-left">
                        <ul className="uk-navbar-nav uk-margin-left">
                            <li className={this.roomsClassName}>
                                <a href='#'  className="uk-button uk-button-text uk-margin-left uk-margin-small-bottom" onClick={this._showRooms}>Manage Rooms</a>
                            </li>
                            <li className={this.wordsClassName}>
                                <a href='#' className="uk-button uk-button-text uk-margin-left uk-margin-small-bottom" onClick={this._showWords}>Manage Words</a>
                            </li>
                        </ul>
                    </div>
                    
                    <div className="uk-navbar-right">
                        <ul className="uk-navbar-nav">
                        <a href='#' className="uk-button uk-button-text uk-margin-right uk-margin-small-bottom" onClick={this._disconnect}>Disconnect</a>
                        </ul>
                    </div>
                </nav>
            </div>
            <div className="uk-margin-left uk-margin-top uk-margin-bottom uk-margin-right">
                <button className="uk-button uk-button-default" onClick={this._refresh}>
                    Refresh
                </button>

                {this._displayRooms()}
                {this._displayWords()}
                {error.error && <p className="uk-text-center uk-text-danger">{error.message}</p>}

            </div>
        </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        token: state.token
    }
}

export default connect(mapStateToProps)(HomePage);