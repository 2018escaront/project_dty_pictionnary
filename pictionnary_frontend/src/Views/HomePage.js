import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import ErrorMessage from '../Components/ErrorMessage';
import { login } from '../utils/Api';

const NO_ERROR = {
    error: false,
    status: 200,
    message: ""
};
const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX = /^(?=.*\d).{4,8}$/;

class HomePage extends React.Component {
    constructor (props) {
        super (props);
        this.state = {
            connectAdmin: false,
            connectUser: false,
            error: NO_ERROR,
            email: "",
            emailClassName: "uk-input uk-text-center",
            password: "",
            passwordClassName: "uk-input uk-text-center",
        }

        // binding de mes fonctions
        this._handlePasswordChange = this._handlePasswordChange.bind(this);
        this._handleEmailChange = this._handleEmailChange.bind(this);
        this._connect = this._connect.bind(this);
        this.handleError = this.handleError.bind(this);
    }
    componentDidMount () {
        // si on a déjà le token, on se connecte
        if (this.props.token) {
            if (this.props.isAdmin) {
                this.setState({
                    connectAdmin: true
                })
            } else {
                this.setState({
                    connectUser: true
                })
            }
        }
    }
    _handlePasswordChange(event) {
            const password = event.target.value;
            let passwordClassName = "uk-input uk-text-center";
            if (!password || !PASSWORD_REGEX.test(password)) {
                passwordClassName += " uk-form-danger";
            };
            this.setState({
                passwordClassName: passwordClassName,
                password: password
            });
    }
    _handleEmailChange(event) {
        const email = event.target.value;
        let emailClassName = "uk-input uk-text-center";
        if (!email || !EMAIL_REGEX.test(email)) {
            emailClassName += " uk-form-danger";
        };
        this.setState({
            emailClassName: emailClassName,
            email: email
        });
    }
    handleError(error){
        // Gestion des erreurs
        console.log(error)
        let status = 500;
        let message = "error occured, please refresh page";
        if (error && error.response) {
            status = error.response.status;
            message = "error : " + error.response.data.error + "; message : " + error.response.data.message;
        } else if (error) {
            message = error;
        }
        
        this.setState({
            error: {
                error: true,
                status: status,
                message: message
            }
        });
    }
    async _connect(event) {
        // Connexion
        event.preventDefault();

        const { email, password } = this.state;

        if (!email || !password ) {
           this.handleError('missing parameters');
        } else if (!EMAIL_REGEX.test(email)) {
            this.handleError('wrong email format');
        } else if (!PASSWORD_REGEX.test(password)) {
            this.handleError('password must be between 4 and 8 chars with at least a number');
        } else {
            const result = await login(email, password, this.handleError)
            const action = { type: "CREATE_USER", value: { token: result.token, username: result.username, userId: result.userId, isAdmin: result.isAdmin } };
            if (result.isAdmin){
                this.props.dispatch(action);
                this.setState({
                    connectAdmin: true
                });
            } else if (result.token) {
                this.props.dispatch(action);
                this.setState({
                    connectUser: true
                });
            }
        }
        
    }

    render() {
        const { error, connectAdmin, connectUser, passwordClassName, emailClassName } = this.state;
        //Error handling
        if (error.error && error.status === 500) {
            return <ErrorMessage status={error.status} message={error.message}/>
        } else if (connectUser) {
            // Connecté comme un user
            return  <Redirect from="/" to="/user" />
        } else if (connectAdmin) {
            // Connecté comme un admin
            return  <Redirect from="/" to="/admin" />
        }
        return (
            
        
        <div className=" uk-container uk-flex uk-flex-center">
        <div className="uk-width-1-3">
            <h3 className="uk-heading-small uk-text-center">Home Page</h3>
            <form onSubmit={this._connect} className="uk-panel uk-panel-box  uk-form-stacked uk-margin-large">
                <div className="uk-margin-small">
                    <label className="uk-form-label uk-text-center" >Email</label>
                    <input 
                        className = {emailClassName}
                        type = "text"
                        placeholder = "example@example.com"
                        onChange = {this._handleEmailChange}
                    />
                </div>
                <div className="uk-margin-small">
                    <label className="uk-form-label uk-text-center" >Password</label>
                    <input 
                        className = {passwordClassName}
                        type = "password"
                        placeholder = "password"
                        onChange = {this._handlePasswordChange}
                    />
                </div>
                <div className="uk-margin-medium">
                    {error.error && <p className="uk-text-center uk-text-danger">{error.message}</p>}
                    <input 
                        className = "uk-input uk-button uk-button-default"
                        type="submit" 
                        value="Connect" 
                    />
                    <div className=" uk-container uk-flex uk-flex-center">
                        <Link to={`/register`}>
                            Create an account
                        </Link>
                    </div>
                </div>
            </form>
        </div>
        </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        token: state.token,
        isAdmin: state.isAdmin
    }
}


export default connect(mapStateToProps)(HomePage);