import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import io  from 'socket.io-client';
import random from 'random';

import { getRoom, getWords, leaveRoom } from '../utils/Api';
import Canvas from '../Components/Canvas';
import ErrorMessage from '../Components/ErrorMessage';

const NO_ERROR = {
    error: false,
    status: 200,
    message: ""
};
const INFOTEXT_INIT = "Waiting for drawer to launch game";
const INFOTEXT_ATTENTE = "Waiting for players to be ready";
const INFOTEXT_JEU = "Try to guess the word!";
const HANDLELANCERTEXT_INIT = "Launch round";
const HANDLELANCERTEXT_ATTENTE = "Interrupt launching";
const HANDLELANCERTEXT_JEU = "Interrupt round";
const NB_POINTS_INIT = 32;

class Room extends React.Component { 
    constructor (props) {
        super (props);

        

        this.socket = io('http://localhost:8080');//, { origins: '*:*'});
        
        
        this._setSecret = this._setSecret.bind(this);
        this._handlePret = this._handlePret.bind(this);
        this._handleLancer = this._handleLancer.bind(this);
        this._handleEssai = this._handleEssai.bind(this);
        this._handleError = this._handleError.bind(this);
        this._handleQuit = this._handleQuit.bind(this);
        this._messageChange = this._messageChange.bind(this);
        this._refresh = this._refresh.bind(this);
        
        this.state = {
            partieName: "Nameless",
            roomId: this.props.match.params.id,
            nbRounds: 0,
            players: 0,
            places: 0,
            nbPoints: NB_POINTS_INIT,
            creatorId: 0,
            isEnded: false,
            nbrDevineurs: 0,
            nbrDevineursPrets: 0,
            listPlayers: [],
            localWinner: false,

            type: "devineur",
            infoText: "Welcome in game",
            handleLancerText: HANDLELANCERTEXT_INIT,

            quit: false,
            etat: "INIT",
            pret: false,
            messages: [],
            error: NO_ERROR,
            alert: null
        };
        
        this.words = null;
        this.secret = "";
        this.message = "";
        
    }
    // Refresh the room information from the database
    async _refresh () {
        const room = await getRoom(this.state.roomId, this.props.token, this._handleError);
        if (room) {
            this.setState({
                partieName: room.partieName,
                nbRounds: room.nbRounds,
                players: room.players,
                places: room.places,
                creatorId: room.creatorId,
                isEnded: room.isEnded,
                listPlayers: room.listPlayers,
                localWinner: room.listPlayers.filter(player => player.id === this.props.userId)[0].localWinner
            });
        }
    }
    componentDidMount () {
        // Verify if user is connected
        if (this.props.token !== "") {
            const roomId = this.props.match.params.id
            // Ask to join the room
            this.socket.emit('join', { roomId: roomId, username: this.props.username, userId: this.props.userId });   
            // Server answer to joining
            this.socket.on('join', function (data) {
                // Server tells if you arrive in a game already started
                if (data.partieDemarree) {
                    this.setState({ 
                        etat: "JEU",
                        handleLancerText: HANDLELANCERTEXT_JEU,
                        infoText: INFOTEXT_JEU
                    });
                }
                // Server tells if you are "drawer" or "devineur"
                if (data.type) {
                    this.setState({ 
                        type: data.type 
                    });
                }
                this._refresh();
            }.bind(this));
            // Server sends information on the drawer's readiness
            this.socket.on("pret", function(data) {
                let newEtat = "";
                let newPret = this.state.pret;
                let handleLancerText = "";
                let infoText = "";
                if (data.pret) {
                    // If the drawer is ready, go to ATTENTE where you can specify your readiness
                    newEtat = "ATTENTE";
                    infoText = INFOTEXT_ATTENTE;
                    handleLancerText = HANDLELANCERTEXT_ATTENTE;
                } else {
                    // If the drawer is not ready, restart the procedure of waiting
                    newEtat = "INIT";
                    infoText = INFOTEXT_INIT;
                    handleLancerText = HANDLELANCERTEXT_INIT;
                    // Set to not ready and inform the server
                    newPret = false;
                    this.socket.emit("pret", { roomId: roomId, pret: newPret, username: this.props.username });
                }
                this.setState({
                    etat: newEtat,
                    pret: newPret,
                    nbPoints: NB_POINTS_INIT,
                    handleLancerText: handleLancerText,
                    infoText: infoText
                })
            }.bind(this));
            // Server tells the game has started
            this.socket.on("partie lancee", function() {
                this.setState({
                    etat: "JEU",
                    handleLancerText: "Interrupt round",
                    infoText: "Try and guess the word!"
                });
                if (this.state.type === "drawer") {
                    this._setSecret();
                }
            }.bind(this));

            this.socket.on("alert", function(data) {
                this.setState({
                    alert: data.message
                })
            }.bind(this));

            // Reception of a message
            // Etat can be [ "info", "drawer quit", "message"]
            this.socket.on('message', function (data) {
                if (data.etat === "info") {
                    // Room information has changed, refresh to see the changes
                    this._refresh();
                } else if (data.etat === "drawer quit") {
                    // No more drawer, the game is ended
                    this._handleQuit();
                }
                // Display the content
                this.setState({
                    messages: [data.sender + " : " + data.message, ...this.state.messages]
                })
            }.bind(this));
        }
    }
    async _setSecret () {
        if (!this.words) {
            this.words = await getWords(this.props.token, this._handleError);
        }
        if (this.words) {
            const l = this.words.length;
            this.secret = this.words[random.int(0,l-1)].word;
            this.setState({
                infoText: "Vous devez faire deviner le mot \""+this.secret+"\""
            })
            this.socket.emit("secret", { roomId: this.state.roomId, word: this.secret });
        }
    }
    _handleError = function (error) {
        // Gestion des erreurs
        console.log(error)
        let status = 500;
        let message = "error occured, please refresh page";
        if (error && error.response) {
            status = error.response.status;
            message = "error : " + error.response.data.error + "; message : " + error.response.data.message;
        } else if (error) {
            message = error;
        }
        
        this.setState({
            error: {
                error: true,
                status: status,
                message: message
            }
        });
    }
    _handleLancer = function() {
        const { etat, roomId } = this.state;
        if (etat === "ATTENTE") {
            this.setState({
                etat: "INIT",
                infoText: INFOTEXT_INIT,
                handleLancerText: HANDLELANCERTEXT_INIT
            });
            this.socket.emit("pret", { roomId: roomId, pret: false, username: this.props.username });
        } else if (etat === "INIT") {
            this.setState({
                etat: "ATTENTE",
                infoText: INFOTEXT_ATTENTE,
                handleLancerText: HANDLELANCERTEXT_ATTENTE
            });
            this.socket.emit("pret", { roomId: roomId, pret: true, username: this.props.username  });
        } else if (etat === "JEU") {
            this.setState({
                etat: "INIT",
                infoText: INFOTEXT_INIT,
                handleLancerText: HANDLELANCERTEXT_INIT
            });
            this.socket.emit("pret", { roomId: roomId, pret: false, username: this.props.username  });
        }
    }
    _handleQuit = function() {
        leaveRoom(this.props.match.params.id, this.state.localWinner, this.props.token, this._handleError);
        this.socket.emit("deco", { username: this.props.username, roomId: this.state.roomId });
        this.setState({
            quit: true
        })
    }
    _handlePret = function() {
        const newPret = !this.state.pret;
        this.socket.emit("pret", { roomId: this.state.roomId, pret: newPret, username: this.props.username });
        this.setState({
            pret: newPret
        });
    }
    _handleEssai = function(e) {
        e.preventDefault();
        if (this.message !== "") {
            const { nbPoints, roomId } = this.state;
            const { userId, username } = this.props;

            if (nbPoints >= 2) {
                this.setState({
                    nbPoints: nbPoints/2
                });
            }
            this.socket.emit("message", { roomId: roomId, message: this.message, userId: userId, nbPoints: nbPoints, sender: username });
            this.message = "";
            e.target.reset();
        }
    }
    _messageChange = function(event) {
        this.message = event.target.value;
    }
    
    

    render() {
        const { error, quit, type, roomId, partieName, players, places, nbRounds, listPlayers, messages, nbPoints, handleLancerText, infoText, pret, etat } = this.state;
        if (error.error) {
            return <ErrorMessage status={error.status} message={error.message} />
        } else if (quit || this.props.token === "" ) {
            const roomRoute = "/room/" + roomId;
            return <Redirect from={roomRoute} to="/user" />
        }
        return (
            <div>
                <div>
                    <nav className="uk-navbar uk-navbar-container">
                        <div className="uk-navbar-left">
                            <ul className="uk-navbar-nav uk-margin-left">
                                <li className="uk-navbar-item uk-logo">Room n°{roomId} "{partieName}"</li>
                                <li className="uk-navbar-item">{players}/{places} players</li>
                                <li className="uk-navbar-item">Il reste : {nbRounds} manches</li>
                            </ul>
                        </div>
                        
                        <div className="uk-navbar-right">
                            <ul className="uk-navbar-nav">
                            <a href='#' className="uk-button uk-button-text uk-margin-right uk-margin-small-bottom" onClick={this._handleQuit}>Quitter le jeu</a>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div className="uk-margin-left uk-margin-top uk-margin-bottom uk-margin-right">
                    <h4 className="uk-text-lead uk-text-center">Votre rôle : {type}</h4>
                    <h5 className="uk-text-lead uk-text-center">{infoText}</h5>
                    <div className="uk-width-1-1 uk-flex">
                        <div className="uk-width-1-6 uk-margin-left uk-margin-top uk-margin-bottom uk-margin-right">
                            <ul className="uk-list uk-list-divider">
                                {listPlayers.map(player => {return <li key={player.id} className="uk-navbar-item">{player.username} has {player.nbLocalPoints}pts (total of {player.nbPoints}pts)</li>})}
                            </ul>
                        </div>
                        <div className="uk-width-1-5 uk-margin-left uk-margin-top uk-margin-bottom uk-margin-right">
                            <div className="uk-panel uk-panel-scrollable" style={{height: '350px'}} >{/*set height 330 */}
                                {messages.map((message, key) => {return <p key={key}>{message}</p>})}
                            </div>
                            {type === "devineur" && 
                                (etat === "ATTENTE" ?
                                    (pret ?
                                        <div>
                                            <p>En attente des autres participants...</p>
                                            <button className="uk-button uk-button-default" onClick={this._handlePret}>Pas Pret</button>
                                        </div>
                                    : 
                                        <div>
                                            <p>Etes-vous pret?</p>
                                            <button className="uk-button uk-button-default" onClick={this._handlePret}>Pret</button>
                                        </div>
                                    )
                                :
                                <form onSubmit={this._handleEssai} className="">
                                    <div className="uk-inline">
                                        <input
                                            className="uk-input"
                                            type = "text"
                                            placeholder = "word"
                                            onChange = {this._messageChange}
                                        />
                                        <input 
                                            className = "uk-button uk-button-default"
                                            type="submit"
                                            value={etat === "INIT" ? "Send Message" : "Try for " + nbPoints + "pts"}
                                        />
                                    </div>
                                </form>
                                )
                            }
                            {type === "drawer" && 
                                <button className="uk-button uk-button-default" onClick={this._handleLancer}>{handleLancerText}</button>
                            }
                            {error.error && <p className="uk-text-center uk-text-danger">{error.message}</p>}
                        </div>
                        <div className=" uk-margin-left uk-margin-top uk-margin-bottom uk-margin-right">
                            <Canvas socket={this.socket} roomId={roomId}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        username: state.username,
        userId: state.userId,
        token: state.token
    }
}


export default connect(mapStateToProps)(Room);