import React from 'react'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'

import { getRooms, joinRoom } from '../utils/Api'
import RoomList from '../Components/RoomList';
import Profile from '../Components/Profile';
import ErrorMessage from '../Components/ErrorMessage'
import CreatePartie from '../Components/CreatePartie';

const NO_ERROR = {
    error: false,
    status: 200,
    message: ""
};
const NUMBER_REGEX = /^([0-9]|[0-9][0-9])$/;

class UserHomePage extends React.Component {
    constructor (props) {
        super (props);

        this.state = {
            joined: false,
            disconnected: false,
            rooms: [],
            status: "rooms",
            error: NO_ERROR
        }
        // Style variables
        this.roomsClassName = "uk-active";
        this.wordsClassName = "";

        // Binding des fonctions
        this.onJoin = this.onJoin.bind(this);
        this.handleError = this.handleError.bind(this);
        this._disconnect = this._disconnect.bind(this);
        this._refresh = this._refresh.bind(this);
        this._showCreateRoom = this._showCreateRoom.bind(this);
        this._showRooms = this._showRooms.bind(this);
        this._showProfile = this._showProfile.bind(this);
    }

    
    handleError(error){
        // Gestion des erreurs
        console.log(error)
        let status = 500;
        let message = "error occured, please refresh page";
        if (error && error.response) {
            status = error.response.status;
            message = "error : " + error.response.data.error + "; message : " + error.response.data.message;
        } else if (error) {
            message = error;
        }
        
        this.setState({
            error: {
                error: true,
                status: status,
                message: message
            }
        });
    }
    async componentDidMount() {
        // Récupération des rooms rejoignables
        this._refresh();
    }


    // Appelée depuis un RoomItem, permet de rejoindre une room
    async onJoin(roomId) {
        // Verification du parametre
        if (!NUMBER_REGEX.test(roomId)) {
            this.handleError("Room Id incorrect");
        } else {
            // Rejoindre la room
            const joined = await joinRoom(roomId, this.props.token, this.handleError);
            this.setState({
                joined: joined,
                roomId: roomId
            });
        }
    }
    // Mise à jour de l'erreur, des rooms et des words
    async _refresh () {
        this.setState({
            error: NO_ERROR
        })
        const rooms = await getRooms(this.props.token, this.handleError);
        this.setState({
            rooms: rooms
        });
    }
    _showCreateRoom () {
        this.createRoomsClassName = "uk-active";
        this.profileClassName = "";
        this.roomsClassName = "";
        this.setState({
            status: "createRoom"
        })
    }
    _showRooms () {
        this.createRoomsClassName = "";
        this.profileClassName = "";
        this.roomsClassName = "uk-active";
        this.setState({
            status: "rooms"
        })
    }
    _showProfile () {
        this.createRoomsClassName = "";
        this.profileClassName = "uk-active";
        this.roomsClassName = "";
        this.setState({
            status: "profile"
        })
    }
    _disconnect () {
        // Deconnexion du user
        this.setState({
            disconnected: true
        })
		const action = { type: "DELETE_USER" };
        this.props.dispatch(action);
    }
    _displayRooms () {
        if (this.state.status === "rooms") {
            const username = this.props.username;
            const { error, rooms } = this.state;
            return (
                <div className="uk-margin-left uk-margin-top uk-margin-bottom uk-margin-right">
                    <h3>Welcome {username}</h3>
                    <div>
                        <button className="uk-button uk-button-default" onClick={this._refresh}>Refresh </button>
                    </div>
                    {error.error && <p className="uk-text-center uk-text-danger">{error.message}</p>}
                    <RoomList 
                        rooms={rooms}
                        onJoin={this.onJoin}
                        type="player"
                    />
                </div>
            )
        }
    }
    _displayCreateRoom () {
        if (this.state.status === "createRoom") {
            return (
                <div className="uk-margin-left uk-margin-top uk-margin-bottom uk-margin-right">
                    <CreatePartie />
                </div>
            )
        }
    }
    _displayProfile () {
        if (this.state.status === "profile") {
            return (
                <div className="uk-margin-left uk-margin-top uk-margin-bottom uk-margin-right">
                    <Profile userId={this.props.userId} token={this.props.token}/>
                </div>
            )
        }
    }

    render() {
        const { error, joined, roomId, disconnected } = this.state;

        // Error handling
        if (error.error && error.status === 500) {
            return <ErrorMessage status={error.status} message={error.message}/>
        } else if (joined) {
            // Handle joining room
            const route = "room/" + roomId;
            return  <Redirect from="/user" to={route} />
        } else if (disconnected || !this.props.token) {
            // Handle disconnection
            return  <Redirect from="/user" to="/" />
        }
        return (
            <div>
                <div>
                    <nav className="uk-navbar uk-navbar-container">
                        <div className="uk-navbar-left">
                            <ul className="uk-navbar-nav uk-margin-left">
                                <li className={this.roomsClassName}>
                                    <a href='#' className="uk-button uk-button-text uk-margin-left uk-margin-small-bottom" onClick={this._showRooms}>Show Rooms</a>
                                </li>
                                <li className={this.createRoomsClassName}>
                                    <a href='#'  className="uk-button uk-button-text uk-margin-left uk-margin-small-bottom" onClick={this._showCreateRoom}>Create Game</a>
                                </li>
                                <li className={this.profileClassName}>
                                    <a href='#'  className="uk-button uk-button-text uk-margin-left uk-margin-small-bottom" onClick={this._showProfile}>See Profile</a>
                                </li>
                            </ul>
                        </div>
                        
                        <div className="uk-navbar-right">
                            <ul className="uk-navbar-nav">
                            <a href='#' className="uk-button uk-button-text uk-margin-right uk-margin-small-bottom" onClick={this._disconnect}>Disconnect</a>
                            </ul>
                        </div>
                    </nav>
                </div>
                {this._displayRooms()}
                {this._displayCreateRoom()}
                {this._displayProfile()}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        token: state.token,
        username: state.username,
        userId: state.userId
    }
}

export default connect(mapStateToProps)(UserHomePage);