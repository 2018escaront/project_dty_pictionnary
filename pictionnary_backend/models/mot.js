'use strict';
module.exports = (sequelize, DataTypes) => {
  const Word = sequelize.define('Word', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    word: {
      allowNull: false,
      type: DataTypes.STRING,
    },
  }, {});
  return Word;
};
