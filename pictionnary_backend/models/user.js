'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING
    },
    username: {
      allowNull: false,
      type: DataTypes.STRING
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING
    },
    isAdmin: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    nbPoints: {
      allowNull: false,
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    nbLocalPoints: {
      allowNull: false,
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    localWinner: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    nbPartiesWon: {
      allowNull: false,
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    nbPartiesPlayed: {
      allowNull: false,
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    nbPartiesHosted: {
      allowNull: false,
      type: DataTypes.INTEGER,
      defaultValue: 0
    }
  }, {});
  
  
  User.associate = function(models) {
    
    // Corresponds to the partie actually played by the player
    models.User.belongsTo(models.Partie, { //adds playingPartieId to User table
      foreignKey: 'playingPartieId',
      allowNull: true,
      targetKey: 'id',
    });
    
  };

  return User;
};


