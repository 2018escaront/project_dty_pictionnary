'use strict';
module.exports = (sequelize, DataTypes) => {
  const Partie = sequelize.define('Partie', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    partieName: {
      allowNull: false,
      type: DataTypes.STRING,
      defaultValue: "Nameless"
    },
    isEnded:{
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
      
    },
    nbRounds:{
      allowNull: false,
      type: DataTypes.INTEGER,
      defaultValue: 3
      
    },
    players:{
      allowNull: false,
      type: DataTypes.INTEGER,
      defaultValue: 1
      
    },
    places:{
      allowNull: false,
      type: DataTypes.INTEGER,
      defaultValue:3
      
    }
  }, {});


  Partie.associate = function(models) {

    //corresponds to the creator of this partie
    models.Partie.belongsTo(models.User, { //adds creatorId to Partie table
      foreignKey: 'creatorId',
      allowNull: false,
      targetKey: 'id'
    });
    
  };
  return Partie;
};