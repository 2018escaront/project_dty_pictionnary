'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Parties', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      //corresponds to the creator of the partie
      creatorId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id',
        }
      },
      partieName: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "Nameless"
      },
      isEnded: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      players: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 1
      },
      places: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 3
      },
      nbRounds: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('Parties');
  }
};