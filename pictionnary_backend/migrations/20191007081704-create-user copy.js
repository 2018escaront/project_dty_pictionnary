'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING
      },
      username: {
        allowNull: false,
        type: Sequelize.STRING
      },
      password: {
        allowNull: false,
        type: Sequelize.STRING
      },
      isAdmin: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      nbPoints: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      nbLocalPoints: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      localWinner: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      nbPartiesWon: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      nbPartiesPlayed: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      nbPartiesHosted: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('Users');
  }
};