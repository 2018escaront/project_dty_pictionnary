'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    
    //adding this column in another migration to link users to parties after having created both tables
    return queryInterface.addColumn('Users', 'playingPartieId', {
      //corresponds to the partie actually played by the player
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue: null,
      references: {
          model: 'Parties',
          key: 'id'
      }
    });
  }
};