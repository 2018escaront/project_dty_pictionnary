// Imports
var models = require('../models');
var jwtUtils = require('../utils/jwt.utils');
var { getPartieUtils } = require('../utils/partieUtils');
var asyncLib = require('async');

// Constants
const PARTIE_NAME_LIMIT = 4;
const NUMBER_REGEX = /^([0-9]|[0-9][0-9])$/;
const TEXT_REGEX = /^([1-zA-Z0-1@.\s]{1,255})$/;

// Routes
module.exports = {

    // Access: User
    // Authorization: token, URL_params: null, body: { partieName: string, places: int, nbRounds: int }
    // Result: { 'id': int }
    // Function: cree une partie
    createPartie: function (req, res) {
        // Getting auth header
        const headerAuth = req.headers['authorization'];
        const userId = jwtUtils.getUserId(headerAuth);

        // Params
        const { partieName, places, nbRounds } = req.body;

        if (partieName == null ||places == null || nbRounds == null) {
            return res.status(400).json({ 'error': 'missing parameters' });
        }
        if (partieName.length <= PARTIE_NAME_LIMIT) {
            return res.status(400).json({ 'error': 'title too small' });
        }
        if (!TEXT_REGEX.test(partieName) || !NUMBER_REGEX.test(places) || !NUMBER_REGEX.test(nbRounds)) {
            return res.status(400).json({ 'error': 'invalid parameters' });
        }
        if (userId < 0) {
            res.status(400).json({ 'error': 'wrong token' });
        }

        asyncLib.waterfall([
            function (done) {
                // Fetch du user par son id de token
                models.User.findOne({
                    attributes: ['id','playingPartieId', 'nbPartiesHosted'],
                    where: { id: userId }
                }).then(function (userFound) {
                    done(null, userFound);
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'unable to verify user' });
                });
            },
            function (userFound, done) {
                // Verify user
                if (userFound) {
                    // Verify user not already playing
                    if (!userFound.playingPartieId){
                        // Create game with user as creator
                        // Set the number of players to one
                        models.Partie.create({
                            partieName: partieName,
                            creatorId: userFound.id,
                            players: 1,
                            places: places,
                            nbRounds: nbRounds
                        }).then(function (partieFound) {
                            done(userFound, partieFound);
                        }).catch(function (error) {
                            return res.status(500).json({'error': error, 'message': "could not create partie"});
                        })
                    } else {
                        return res.status(400).json({ 'error': 'user already playing' });
                    }
                } else {
                    return res.status(404).json({ 'error': 'user not found' });
                }
            }
        ], function (userFound, partieFound) {
            // Update user's playing partie and his number of games hosted
            userFound.update({
                playingPartieId: partieFound.id,
                nbPartiesHosted: userFound.nbPartiesHosted + 1,
            }).then(function () {
                return res.status(201).json({ 'id': partieFound.id });
            }).catch(function (err) {
                return res.status(500).json({'error': err, 'message': 'unable to update user'});
            });
        });
        
    },











    // Access: User
    // Authorization: token, URL_params: partieId, body: null
    // Result: { id: int, partieName: string, creatorId: int, isEnded: boolean, players: int, places: int, nbRounds: int, listPlayers: { objUser } }
    // Function: liste les parametres d'une partie
    getPartie: async function(req, res) {
        // Authentification
        var headerAuth = req.headers['authorization'];
        var userId = jwtUtils.getUserId(headerAuth);
        // Parametres
        var partieId = req.params.partieId;
        if (partieId == null || !NUMBER_REGEX.test(partieId)) {
            return res.status(400).json({ 'error': 'wrong parameters' });
        }
        if (userId < 0) {
            res.status(400).json({ 'error': 'wrong token' });
        }
        // Recherche du user par son token
        models.User.findOne({
            attributes: [ 'id' ],
            where: { id: userId }
        }).then(async function (userFound) {
            // Verification de l'existence du user
            if (userFound) {
                const result = await getPartieUtils(partieId);
                return res.status(result.status).json(result.result);
            } else {
                return res.status(404).json({ 'error': 'user not found' });
            }
        }).catch(function (err) {
            console.log(err)
            return res.status(500).json({ 'error': err, 'message':'unable fetch user' });
        });
    },




    // Access: user
    // Authorization: token, URL_params: null, Body: null
    // Result: [{ id: int, partieName: string, creatorId: int, isEnded: boolean, players: int, places: int, nbRounds: int, createdAt: date, user: objectUser }]
    // Function: renvoie toutes les parties not ended
    listParties: function(req, res) {
        // Authentification
        var headerAuth = req.headers['authorization'];
        var userId = jwtUtils.getUserId(headerAuth);
        if (userId < 0) {
            return res.status(400).json({ 'error': 'wrong token' });
        }
        asyncLib.waterfall([
            function (done) {
                // Recherche du user par son token
                models.User.findOne({
                    attributes: [ 'id' ],
                    where: { id: userId }
                }).then(function (userFound) {
                    done(userFound);
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message':'unable to verify user' });
                });
            }
        ], function(userFound){
            // Verification de l'existence du user
            if (userFound){
                // Liste des parties en cours
                models.Partie.findAll({
                    attributes: ['id', 'partieName','creatorId','isEnded', 'players', 'places', 'nbRounds','createdAt'],
                    include: ['User'],
                    where: {isEnded: false}
                }).then(function (parties) {
                    if (parties) {
                        return res.status(200).json(parties)
                    } else {
                        return res.status(404).json({ 'error': 'no parties found' });
                    }
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'invalid fields' });
                });
            } else {
                return res.status(404).json({ 'error': 'user not found' });
            }
        });
    },





    // Access: Admin
    // Authorization: token, URL_params: null, Body: null
    // Result: [{ id: int, partieName: string, creatorId: int, isEnded: boolean, players: int, places: int, nbRounds: int, createdAt: date }]
    // Function: renvoie toutes les parties
    listPartiesAll: function(req, res) {
        // Authentification
        var headerAuth = req.headers['authorization'];
        var userId = jwtUtils.getUserId(headerAuth);
        var isAdmin = jwtUtils.getIsAdmin(headerAuth);
        console.log(userId, isAdmin)

        if (userId < 0) {
            return res.status(400).json({ 'error': 'wrong token' });
        }
        if (!isAdmin) {
            return res.status(400).json({ 'error': 'user not admin' });
        }

        asyncLib.waterfall([
            function (done) {
                // Recherche du user par son token
                models.User.findOne({
                    attributes: [ 'id' ],
                    where: { id: userId }
                }).then(function (userFound) {
                    done(userFound);
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'unable to verify user' });
                });
            }
        ], function(userFound){
            // Verification de l'existence du user
            if (userFound){
                models.Partie.findAll({
                    attributes: ['id', 'partieName','creatorId','isEnded', 'players', 'places', 'nbRounds', 'createdAt']
                }).then(function (parties) {
                    if (parties) {
                        return res.status(200).json(parties)
                    } else {
                        return res.status(404).json({ 'error': 'no parties found' });
                    }
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'invalid fields' });
                });
            } else {
                return res.status(400).json({ 'error': 'user does not exist' });
            }
        });
    },













    // Access: User
    // Authorization: token, URL_params: userId, Body: null
    // Result: [{ id: int, partieName: string, creatorId: int, isEnded: boolean, players: int, places: int, nbRounds: int, createdAt: date }]
    // Function: renvoie toutes les parties crées par le user
    listPartiesCrees: function(req, res) {
        const order = req.query.order;
        
        // Getting auth header
        const headerAuth = req.headers['authorization'];
        const userId = jwtUtils.getUserId(headerAuth);
        const isAdmin = jwtUtils.getIsAdmin(headerAuth);

        const userIdURL = parseInt(req.params.userId);
        if (userId < 0) {
            return res.status(400).json({ 'error': 'wrong token' });
        }
        // The user is admin or he asks his own profile
        if (userId != userIdURL && !isAdmin) {
            return res.status(403).json({ 'error': 'user not admin' });
        }

        asyncLib.waterfall([
            function (done) {
                // Fetch user
                models.User.findOne({
                    attributes: [ 'id' ],
                    where: { id: userId }
                }).then(function (userFound) {
                    done(userFound);
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'unable to verify user' });
                });
            }
        ], function(userFound){
            // Verify user existence
            if (userFound){
                // Fetch games created by userIdURL
                models.Partie.findAll({
                    order: [(order != null) ? order.split(':') : ['createdAt', 'ASC']],
                    attributes:  ['id', 'partieName','creatorId','isEnded', 'players', 'places', 'nbRounds', 'createdAt'],
                    include: ['User'],
                    where : {creatorId: userIdURL}
                }).then(function (parties) {
                    if (parties) {
                        return res.status(200).json(parties);
                    } else {
                        return res.status(404).json({ 'error': 'no parties found' });
                    }
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'invalid fields' });
                });
            } else {
                return res.status(400).json({ 'error': 'user does not exist' });
            }
        });
    },























    // Access: User
    // Authorization: token, URL_params: idRoom, Body: null
    // Result: null
    // Function: update a user's playingPartieId and nbPartiesPlayed, sets localWinner to false
    joinPartie: function (req, res) {
        var headerAuth = req.headers['authorization'];
        var userId = jwtUtils.getUserId(headerAuth);

        // Params
        var partieId = parseInt(req.params.partieId);
        if (partieId == null || !NUMBER_REGEX.test(partieId)) {
            return res.status(400).json({ 'error': 'wrong parameters' });
        }
        if (userId < 0) {
            res.status(400).json({ 'error': 'wrong token' });
        }

        asyncLib.waterfall([
            function (done) {
                // Fetch le user
                models.User.findOne({
                    attributes: ['id', 'playingPartieId', 'localWinner', 'nbPartiesPlayed'],
                    where: { id: userId }
                }).then(function (userFound) {
                    done(null, userFound);
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'unable to verify user' });
                });
            },
            function (userFound, done) {
                // Verify user existence
                if (userFound) {
                    // Fetch partie
                    models.Partie.findOne({
                        attributes: ['id', 'isEnded', 'players', 'places'],
                        where: { id: partieId }
                    }).then(function (partieFound) {
                        done(null,  userFound, partieFound);
                    }).catch(function (err) {
                        return res.status(500).json({ 'error': err, 'message': 'unable to verify partie'});
                    });
                } else {
                    return res.status(404).json({ 'error': 'user not found' });
                }
            },
            function (userFound, partieFound, done) {
                // Verify partie existence
                if (partieFound) {
                    //Verify partie playable
                    if (!partieFound.isEnded) {
                        // Verify user not already playing
                        if (!userFound.playingPartieId){
                            // Verify space free to join partie
                            if (partieFound.players < partieFound.places) {
                                // Update user's playing partie and number of games played, reset localWinner to false
                                userFound.update({
                                    playingPartieId: partieFound.id,
                                    nbPartiesPlayed: userFound.nbPartiesPlayed+1,
                                    localWinner: false
                                }).then(function () {
                                    done(partieFound);
                                }).catch(function (err) {
                                    return res.status(500).json({'error': err, 'message': 'unable to update user'});
                                });
                            } else {
                                return res.status(400).json({'error': 'partie full'});
                            }
                        } else {
                            return res.status(400).json({'error': 'user already playing'});
                        }
                    } else {
                        return res.status(400).json({ 'error': 'Partie ended'});
                    }
                } else {
                    return res.status(404).json({ 'error': 'cannot find partie' });
                }
            }
        ], function (partieFound) {
            // Update game's number of players
            partieFound.update({
                players: partieFound.players + 1
            }).then(function () {
                return res.status(200).json();
            }).catch(function (err) {
                return res.status(500).json({'error': err, 'message': 'unable to update partie'});
            });
        });
    },













    // Access: User
    // Authorization: token, URL_params: idRoom, Body: won: boolean
    // Result: null
    // Function: update a user playingPartieId to null and if won, increment nbPartiesWon
    leavePartie: function (req, res) {
        // Getting auth header
        const headerAuth = req.headers['authorization'];
        const userId = jwtUtils.getUserId(headerAuth);

        // Verify params
        const won = req.body.won;
        const partieId = parseInt(req.params.partieId);
        if (partieId == null || !NUMBER_REGEX.test(partieId)) {
            return res.status(400).json({ 'error': 'wrong parameters' });
        }
        if (userId < 0) {
            res.status(400).json({ 'error': 'wrong token' });
        }
        asyncLib.waterfall([
            function (done) {
                // Fetch user
                models.User.findOne({
                    attributes: ['id','playingPartieId', 'nbPartiesWon'],
                    where: { id: userId }
                }).then(function (userFound) {
                    done(null, userFound);
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'unable to verify user' });
                });
            },
            function (userFound, done) {
                // Verify user existence
                if (userFound) {
                    // Fetch partie
                    models.Partie.findOne({
                        attributes: ['id', 'creatorId', 'isEnded', 'partieName'],
                        where: { id: partieId }
                    }).then(function (partieFound) {
                        done(null, userFound, partieFound);
                    }).catch(function (err) {
                        console.log(err);
                        return res.status(500).json({ 'error': err, 'message': 'unable to verify partie'});
                    });
                } else {
                    return res.status(404).json({ 'error': 'user not found' });
                }
            },
            function (userFound, partieFound, done) {
                // Verify partie existence
                if (partieFound) {
                    // Verify if player is playing
                    if (userFound.playingPartieId != null){
                        // Set player's playingPartie to null
                        // If he won, increment nbPartiesWon
                        if (won) {
                            userFound.update({
                                playingPartieId: null,
                                nbPartiesWon: userFound.nbPartiesWon+1
                            }).then(function () {
                                done(partieFound, userFound);
                            }).catch(function (err) {
                                return res.status(500).json({'error': err, 'message': 'unable to update user'});
                            });
                        } else {
                            userFound.update({
                                playingPartieId: null
                            }).then(function () {
                                done(partieFound, userFound);
                            }).catch(function (err) {
                                return res.status(500).json({'error': err, 'message': 'unable to update user'});
                            });
                        }
                    } else {
                        return res.status(400).json({'error': 'user already not playing'});
                    }
                } else {
                    return res.status(404).json({ 'error': 'cannot find partie' });
                }
            },
            
        ], function (partieFound, userFound) {
            // Verify if userFound is creator
            if (partieFound.creatorId == userFound.id) {
                // Set partie as ended
                partieFound.update({
                    isEnded: true
                }).then(function () {
                    return res.status(200).json();
                }).catch(function (err) {
                    return res.status(500).json({'error': err, 'message': 'unable to update partie'});
                });
            } else {
                return res.status(200).json();
            }
        });
    },













    // Access: Admin
    // Authorization: token, URL_params: idRoom, Body: null
    // Result: [{ id: int, partieName: string, creatorId: int, isEnded: boolean, players: int, places: int, nbRounds: int }]
    // Function: update un user pour qu'il quitte une partie
    deletePartie: function (req, res) {
        // Getting auth header
        var headerAuth = req.headers['authorization'];
        var userId = jwtUtils.getUserId(headerAuth);
        var isAdmin = jwtUtils.getIsAdmin(headerAuth);

        // Params
        var partieId = parseInt(req.params.partieId);
        if (userId < 0) {
            res.status(400).json({ 'error': 'wrong token' });
        }
        if (!isAdmin) {
            return res.status(400).json({ 'error': 'user not admin' });
        }
        if (partieId == null || !NUMBER_REGEX.test(partieId)) {
            return res.status(400).json({ 'error': 'wrong parameters' });
        }
        asyncLib.waterfall([
            function (done) {
                // Fetch user
                models.User.findOne({
                    attributes: ['id'],
                    where: { id: userId }
                }).then(function (userFound) {
                    done(null, userFound);
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'unable to verify user' });
                });
            },
            function (userFound, done) {
                // Verify user existence
                if (userFound) {
                    // Fetch partie to delete
                    models.Partie.findOne({
                        attributes: ['id'],
                        where: { id: partieId }
                    }).then(function (partieFound) {
                        done(null, partieFound);
                    }).catch(function (err) {
                        return res.status(500).json({ 'error': err, 'message': 'unable to verify partie' });
                    });
                } else {
                    return res.status(404).json({ 'error': 'user not found' });
                }
            },
            function (partieFound, done) {
                // a vérifier que ya pas de players en train de jouer sinon l'action est inefficace
                models.User.findOne({
                    attributes: ['id','playingPartieId'],
                    where: { playingPartieId: partieId }
                }).then(function (playersFound) {
                    if (playersFound){
                        return res.status(400).json({ 'error': 'still players playing', 'players':playersFound});
                    } else {
                        done(null, partieFound);
                    }
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'unable to verify players of the partie' });
                });
            },
            function (partieFound, done) {
                // Verify the existence of the game
                if (partieFound) {
                    // Destroy the game
                    models.Partie.destroy({
                        where: {id: partieId}
                    }).then(function() {
                        done();
                    }).catch(function(err) {
                        return res.status(500).json({'error': err, 'message': 'unable to delete partie'});
                    })
                } else {
                    return res.status(404).json({ 'error': 'cannot find partie' });
                }
            }
        ], function () {
            // Return all rooms
            models.Partie.findAll({
                attributes: ['id', 'partieName','creatorId','isEnded', 'players', 'places', 'nbRounds']
            }).then(function (parties) {
                if (parties) {
                    return res.status(200).json(parties)
                } else {
                    return res.status(404).json({ 'error': 'no parties found' });
                }
            }).catch(function (err) {
                return res.status(500).json({ 'error': err, 'message': 'invalid fields' });
            });
        });
    },




    emptyPartie: function (req, res) {
        // const headerAuth = req.session.token;
        var headerAuth = req.headers['authorization'];
        var userId = jwtUtils.getUserId(headerAuth);
        var isAdmin = jwtUtils.getIsAdmin(headerAuth);

        // Params
        var partieId = parseInt(req.params.partieId);
        if (userId < 0) {
            res.status(400).json({ 'error': 'wrong token' });
        }
        if (partieId == null) {
            return res.status(400).json({ 'error': 'missing parameters' });
        }
        if (NUMBER_REGEX.test(partieId)) {
            return res.status(400).json({ 'error': 'invalid parameters' });
        }
        if (!isAdmin) {
            return res.status(400).json({ 'error': 'user not admin' });
        }
        asyncLib.waterfall([
            function (done) {
                models.User.findOne({
                    attributes: ['id'],
                    where: { id: userId }
                }).then(function (userFound) {
                    done(null, userFound);
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'unable to verify user' });
                });
            },
            function (userFound, done) {
                if (userFound) {
                    models.Partie.findOne({
                        attributes: ['id'],
                        where: { id: partieId }
                    }).then(function () {
                        done();
                    }).catch(function (err) {
                        console.log(err);
                        return res.status(500).json({ 'error': err, 'message': 'unable to verify partie' });
                    });
                } else {
                    return res.status(404).json({ 'error': 'user not found' });
                }
            },
            
        ], function () {
            // a vérifier que ya pas de players en train de jouer sinon l'action est inefficace
            models.User.findAll({
                attributes: ['id','playingPartieId'],
                where: { playingPartieId: partieId }
            }).then(function (playersFound) {
                for (var indice in playersFound) {
                    playersFound[indice].update({
                        'playingPartieId': null,
                    }).catch(function(err) {
                        console.log(err);
                        return res.status(500).json({ 'error': 'unable to update players of the partie' });
                    });
                }
                return res.status(201).json(playersFound)
            }).catch(function (err) {
                return res.status(500).json({ 'error': err,'message': 'unable to verify players of the partie' });
            });
        });
    }

}
