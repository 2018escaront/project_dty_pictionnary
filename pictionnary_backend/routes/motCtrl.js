// Imports
var jwtUtils = require('../utils/jwt.utils');
var models = require('../models');
var asyncLib = require('async');


const WORD_REGEX = /^([a-z]{1,})$|^$/;

// Routes
module.exports = {
    // Access: Admin
    // Authorization: token, URL_params: null, Body: word
    // Result: [{ id: int, word: string }]
    // Function: ajoute un mot et renvoie la liste de tous les mots
    addWord: function(req, res) {
        // Getting auth header
        var headerAuth = req.headers['authorization'];
        var userId = jwtUtils.getUserId(headerAuth);
        var isAdmin = jwtUtils.getIsAdmin(headerAuth);

        // Params
        var word = req.body.word;
        
        if (!isAdmin) {
            return res.status(400).json({ 'error': 'user not admin' });
        }
        if (!word) {
            return res.status(400).json({ 'error': 'missing parameters' });
        }
        if (!WORD_REGEX.test(word)) {
            return res.status(400).json({ 'error': 'word contains illegal characters' });
        }

        asyncLib.waterfall([
            function (done) {
                // Fetch user
                models.User.findOne({
                    attributes: ['id'],
                    where: { id: userId }
                }).then(function (userFound) {
                    done(null, userFound);
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'unable to find user' });
                });
            },
            function (userFound, done) {
                // Verify user existence
                if (userFound) {
                    // Fetch same word
                    models.Word.findOne({
                        attributes: ['id', 'word'],
                        where: { word: word }
                    }).then(function (wordFound) {
                        done(null, wordFound);
                    }).catch(function (err) {
                        return res.status(500).json({ 'error': err, 'message': 'unable to check word' });
                    });
                } else {
                    return res.status(404).json({ 'error': 'user not found' });
                }
            },
            function (wordFound, done) {
                // Verify word doesn't exist
                if (!wordFound) {
                    // Create word
                    models.Word.create({
                        word: word
                    }).then(function () {
                        done();
                    }).catch(function(err) {
                        return res.status(500).json({ 'error': err, 'message': 'unable to add word' });
                    });
                } else {
                    return res.status(400).json({ 'error': 'word already exist' });
                }
            }

        ], function () {
            // Fetch all words
            models.Word.findAll({
                attributes: ['id', 'word']
            }).then(function (words) {
                // Verify words existence
                if (words) {
                    return res.status(200).json(words)
                } else {
                    return res.status(404).json({ 'error': 'no words found' });
                }
            }).catch(function (err) {
                return res.status(500).json({ 'error': err, 'message': 'could not fetch words' });
            });
        });
    },
















    // Access: User
    // Authorization: token, URL_params: null, Body: null
    // Result: [{ id: int, word: string }]
    // Function: liste tous les mots
    listWords: function (req, res) {
        // Getting auth header
        var headerAuth = req.headers['authorization'];
        var userId = jwtUtils.getUserId(headerAuth)


        asyncLib.waterfall([
            function (done) {
                // Fetch le user
                models.User.findOne({
                    attributes: ['id'],
                    where: { id: userId }
                }).then(function (userFound) {
                    done(userFound);
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'unable to find user' });
                });
            },
        ], function (userFound) {
            // Verify user existence
            if (userFound) {
                // Fetch all words
                models.Word.findAll({
                    attributes: ['id', 'word']
                }).then(function (words) {
                    // Verify words existence
                    if (words) {
                        return res.status(200).json(words)
                    } else {
                        return res.status(404).json({ 'error': 'no words found' });
                    }
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'could not fetch words' });
                });
            } else {
                return res.status(404).json({ 'error': 'user not found' });
            }
        });
    },













    // Access: Admin
    // Authorization: token, URL_params: null, Body: word
    // Result: [{ id: int, word: string }]
    // Function: ajoute un mot et renvoie la liste de tous les mots
    deleteWord: function(req, res) {
        var headerAuth = req.headers['authorization'];
        var userId = jwtUtils.getUserId(headerAuth);
        var isAdmin = jwtUtils.getIsAdmin(headerAuth);

        // Params
        var word = req.body.word;
        // Verifications
        if (!isAdmin) {
            return res.status(400).json({ 'error': 'user not admin' });
        }
        if (!word) {
            return res.status(400).json({ 'error': 'missing parameters' });
        }
        if (!WORD_REGEX.test(word)) {
            return res.status(400).json({ 'error': 'word contains illegal characters' });
        }
        asyncLib.waterfall([
            function (done) {
                // Fetch user
                models.User.findOne({
                    attributes: ['id'],
                    where: { id: userId }
                }).then(function (userFound) {
                    done(null, userFound);
                }).catch(function (err) {
                    console.log(err);
                    return res.status(500).json({ 'error': 'unable to verify user' });
                });
            },
            function (userFound, done) {
                // Verify user existence
                if (userFound) {
                    // Fetch word to supress
                    models.Word.findOne({
                        attributes: ['id', 'word'],
                        where: { word: word }
                    }).then(function (wordFound) {
                        done(null, wordFound);
                    }).catch(function (err) {
                        return res.status(500).json({ 'error': err, 'message': 'unable to verify word' });
                    });
                } else {
                    return res.status(404).json({ 'error': 'user not found' });
                }
            },
            function (wordFound, done) {
                // Verify word to supress exists
                if (wordFound) {
                    // Supress word
                    models.Word.destroy({
                        where: {word: word}
                    }).then(function() {
                        done();
                    }).catch(function(err) {
                        return res.status(500).json({'error': err, 'message': 'unable to delete word'});
                    })
                } else {
                    return res.status(404).json({ 'error': 'cannot find word' });
                }
            }
        ], function () {
            // Fetch remaining words
            models.Word.findAll({
                attributes: ['id', 'word']
            }).then(function (words) {
                // Verify words existence
                if (words) {
                    return res.status(200).json(words)
                } else {
                    return res.status(404).json({ 'error': 'no words found' });
                }
            }).catch(function (err) {
                return res.status(500).json({ 'error': err, 'message': 'could not fetch words' });
            });
        });
    }
};