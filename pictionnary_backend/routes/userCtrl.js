// Imports
var bcrypt = require('bcrypt');
var jwtUtils = require('../utils/jwt.utils');
var models = require('../models');
var asyncLib = require('async');


// Constants
const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX = /^(?=.*\d).{4,8}$/;
const TEXT_REGEX = /^([1-zA-Z0-1@.\s]{1,255})$/;

/*
Object shape

objectUser = { id: int, email: string, username: string, password: string, isAdmin: boolean, nbPartiesWon: int, nbPartiesPlayed: int, playingPartieId: int}



*/

// Routes
module.exports = {
    
    // Access: all
    // Authorization: null, URL_params: null, body: { email: string, username: string, password: string, confirmPassword: string }
    // Result: { 'userId': int }
    // Function: cree un user
    register: function(req, res) {
        const { email, username, password, confirmPassword } = req.body;
        
        if (email == "" || username == "" || password == "" || email == null || password == null || username == null) {
            return res.status(400).json({ 'error': 'missing parameters' });
        }
        if (username.length >= 13 || username.length <= 4) {
            return res.status(400).json({ 'error': 'username need between 5 and 12 characters' });
        }
        if (!EMAIL_REGEX.test(email)) {
            return res.status(400).json({ 'error': 'wrong email format' });
        }
        if (!PASSWORD_REGEX.test(password)) {
            return res.status(400).json({ 'error': 'password must be between 4 and 8 chars with at least a number' });
        }
        if (password !== confirmPassword) {
            return res.status(400).json({ 'error': "confirmation password doesn't match password" });
        }
        if (!TEXT_REGEX.test(username)) {
            return res.status(400).json({ 'error': "invalid parameters" });
        }

        
        asyncLib.waterfall([
            function (done) {
                models.User.findOne({
                    attributes: ['email'],
                    where: { email: email }
                }).then(function (userFound) {
                    done(null, userFound);
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'unable to verify user'});
                });
            },
            function (userFound, done) {
                if (!userFound) {
                    bcrypt.hash(password, 5, function (err, bcryptedPassword) {
                        done(null, userFound, bcryptedPassword);
                    });
                } else {
                    return res.status(409).json({ 'error': 'user already exists' });
                }
            },

            function (userFound, bcryptedPassword, done) {
                models.User.create({
                    email: email,
                    username: username,
                    password: bcryptedPassword
                }).then(function (newUser) {
                    done(newUser);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({ 'error': err, 'message':'cannot create user' });
                });
            }
        ], function (newUser) {
            if (newUser) {
                return res.status(201).json({
                    'userId': newUser.id
                });
            } else {
                return res.status(500).json({ 'error': 'cannot add user' });
            }
        });
    },



    // Access: all
    // Authorization: null, URL_params: null, body: { email: string, password: string }
    // Result: { 'user': objectUser, 'token': string }
    // Function: renvoie le token d'un user enregistré
    login: function (req, res) {
        
        //Parametres
        const email = req.body.email;
        const password = req.body.password;

        if (!email || !password ) {
            return res.status(400).json({ 'error': 'missing parameters' });
        }
        if (!EMAIL_REGEX.test(email)) {
            return res.status(400).json({ 'error': 'wrong email format' });
        }
        if (!PASSWORD_REGEX.test(password)) {
            return res.status(400).json({ 'error': 'password must be between 4 and 8 chars with at least a number' });
        }

        asyncLib.waterfall([
            function (done) {
                models.User.findOne({
                    attributes: ['id', 'password', 'playingPartieId', 'isAdmin', 'username'],
                    where: { email: email }
                }).then(function (userFound) {
                    done(null, userFound);
                }).catch(function (err) {
                    return res.status(500).json({ 'error': err, 'message': 'unable to verify user'});
                });
            },
            function (userFound, done) {
                if (userFound) {
                    bcrypt.compare(password, userFound.password, function (errBcrypt, resBcrypt) {
                        done(null, userFound, resBcrypt);
                    });
                } else {
                    return res.status(404).json({ 'error': 'user not exists in DB' });
                }
            },
            function (userFound, resBcrypt, done) {
                if (resBcrypt) {
                    if (userFound.playingPartieId) {
                        userFound.update({
                            playingPartieId: null
                        }).then(function () {
                            done(userFound);
                        }).catch(function (err) {
                            return res.status(500).json({ 'error': err });
                        });
                    } else {
                        done(userFound);
                    }
                } else {
                    return res.status(400).json({ 'error': "invalid password" });
                }
            }
        ],  function (userFound) {
            if (userFound) {
                var token = jwtUtils.generateTokenForUser(userFound);
                return res.status(201).json({
                    'username': userFound.username,
                    'userId': userFound.id,
                    'token': token,
                    'isAdmin': userFound.isAdmin
                });
            } else {
                return res.status(500).json({ 'error': 'cannot log user' });
            }
        });
    },





    // Access: User
    // Authorization: token, URL_params: userId, body: null
    // Result: { 'id', 'email', 'username', 'playingPartieId', 'nbPoints', 'nbLocalPoints', 'nbPartiesWon', 'nbPartiesPlayed', 'nbPartiesHosted', 'isAdmin' }
    // Function: gives back the user asked in url
    getUser: function(req, res) {
        // Getting auth header
        var headerAuth = req.headers['authorization'];
        var userId = jwtUtils.getUserId(headerAuth);
        var isAdmin = jwtUtils.getIsAdmin(headerAuth);

        var userIdURL = parseInt(req.params.userId);
        if (userId < 0) {
            res.status(400).json({ 'error': 'wrong token' });
        }
        // The user is admin or he asks his own profile
            if (userId != userIdURL && !isAdmin) {
            res.status(403).json({ 'error': 'user not admin' });
        }

        // Fetch user from id in URL
        models.User.findOne({
            attributes: ['id', 'email', 'username', 'playingPartieId', 'nbPoints', 'nbLocalPoints', 'nbPartiesWon', 'nbPartiesPlayed', 'nbPartiesHosted', 'isAdmin'],
            where: {id: userIdURL}
        }).then(function (userFound) {
            // Verify user existence
            if (userFound) {
                // Send back the user
                res.status(200).json({
                    'id': userFound.id,
                    'email':  userFound.email,
                    'username':  userFound.username,
                    'playingPartieId':  userFound.playingPartieId,
                    'nbPartiesWon':  userFound.nbPartiesWon,
                    'nbPartiesPlayed':  userFound.nbPartiesPlayed,
                    'nbPoints':  userFound.nbPoints,
                    'nbLocalPoints':  userFound.nbLocalPoints,
                    'nbPartiesHosted':  userFound.nbPartiesHosted,
                    'isAdmin':  userFound.isAdmin
                });
            } else {
                res.status(404).json({ 'error': 'user not found' });
            }
        })
        .catch(function (err) {
            res.status(500).json({ 'error': err, 'message': 'cannot fetch user' });
        })
    }
};