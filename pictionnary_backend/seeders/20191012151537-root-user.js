'use strict';
var bcryptedPassword = "$2b$05$9PMuo4./0o3AfqcqX2AqmO1iTSmcPLGiyl1bZYDNxRAOFhT/lz0/K";// 5 rounds to code root123


module.exports = {
  
  up: (queryInterface) => {
    return queryInterface.bulkInsert('Users', [{
        username: 'root',
        email: 'root@root.fr',
        isAdmin: 1,
        playingPartieId: null,
        password: bcryptedPassword,
        nbPoints: 0,
        nbLocalPoints: 0,
        nbPartiesPlayed: 0,
        nbPartiesWon: 0,
        nbPartiesHosted: 0,
        createdAt: new Date(),
        updatedAt: new Date()
      }], {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
