
var models = require('../models');


module.exports = {
    // Parameters: { userId: int, points: int }
    // Result: null
    // Function: ajoute des points à un user
    updateUserPointsUtils: function(userId, points) {
        // Fetch user
        models.User.findOne({
            attributes: ['id', 'nbPoints', 'nbLocalPoints'],
            where: { id: userId }
        }).then(function (userFound) {
            if (points === -1) {
                // Reset local points for a new game
                userFound.update({
                    nbLocalPoints: 0
                }).catch(function (err) {
                    console.log(err);
                });
            } else {
                // Update points and local points
                userFound.update({
                    nbPoints: userFound.nbPoints+points,
                    nbLocalPoints: userFound.nbLocalPoints+points
                }).catch(function (err) {
                    console.log(err);
                });
            }
        }).catch(function (err) {
            console.log(err);
        });
    },
    updateWinnerUtils: function(userId) {
        // Fetch user
        models.User.findOne({
            attributes: ['id', 'localWinner'],
            where: { id: userId }
        }).then(function (userFound) {
            // Reset local points for a new game
            userFound.update({
                localWinner: true
            }).catch(function (err) {
                console.log(err);
            });
        }).catch(function (err) {
            console.log(err);
        });
    },

    // // Parameters: { userId: int, partieWon: boolean }
    // // Result: null
    // // Function: ajoute une partie jouée au user, si partieWon, ajoute une partie gagnée
    // updateUserPartieUtils: function(userId, partieWon) {
    //     // Fetch user
    //     models.User.findOne({
    //         attributes: ['id', 'nbPartiesWon', 'nbPartiesPlayed'],
    //         where: { id: userId }
    //     }).then(function (userFound) {
    //         // If he is the best of the game, add a partie Won
    //         // In all cases, add a game played
    //         if (partieWon) {
    //             userFound.update({
    //                 nbPartiesPlayed: userFound.nbPartiesPlayed+1,
    //                 nbPartiesWon: userFound.nbPartiesWon+1
    //             }).catch(function (err) {
    //                 console.log(err);
    //             });
    //         } else {
    //             userFound.update({
    //                 nbPartiesPlayed: userFound.nbPartiesPlayed+1,
    //             }).catch(function (err) {
    //                 console.log(err);
    //             });
    //         }
    //     }).catch(function (err) {
    //         console.log(err);
    //     });
    // },

    // // Parameters: { userId: int }
    // // Result: null
    // // Function: adds a hosted game to the user
    // updateUserHostsUtils: function(userId) {
    //     // Fetch user
    //     models.User.findOne({
    //         attributes: ['id', 'nbPartiesWon', 'nbPartiesHosted'],
    //         where: { id: userId }
    //     }).then(function (userFound) {
    //         userFound.update({
    //             nbPartiesHosted: userFound.nbPartiesHosted+1,
    //         }).catch(function (err) {
    //             console.log(err);
    //         });
    //     }).catch(function (err) {
    //         console.log(err);
    //     });
    // }
}