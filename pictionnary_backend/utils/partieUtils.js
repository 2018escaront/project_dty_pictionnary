
var models = require('../models');

module.exports = {
    // Parameters: { partieId }
    // Result: { status: int, result: { id: int, partieName: string, creatorId: int, isEnded: boolean, players: int, localWinner: boolean, places: int, nbRounds: int, listPlayers: { objUser } } }
    // Function: liste les parametres d'une partie
    getPartieUtils: async function (partieId) {
        // Récupération des joueurs
        
        return models.User.findAll({
            attributes: ['id', 'username', 'nbPoints', 'nbLocalPoints', 'localWinner'],
            where: { playingPartieId: partieId }
        }).then(function (listPlayers) {
            // Fetch partie
            return models.Partie.findOne({
                attributes: ['id', 'partieName','creatorId','isEnded', 'players', 'places', 'nbRounds'],
                where: { id: partieId }
            }).then(function (partieFound) {
                // Verify partie existence
                if (partieFound) {
                    return { status: 200, result: {
                        'id': partieFound.id,
                        'partieName': partieFound.partieName,
                        'creatorId': partieFound.creatorId,
                        'isEnded': partieFound.isEnded,
                        'nbRounds': partieFound.nbRounds,
                        'localWinner': partieFound.nbRounds,
                        'players': partieFound.players,
                        'places': partieFound.places,
                        'listPlayers': listPlayers
                    }};
                } else {
                    return { status: 404, result: { 'error': 'no parties found' }};
                }
            }).catch(function (err) {
                return { status: 500, result: { 'error': err, 'message': 'invalid fields' }};
            });
        }).catch(function (err) {
            return { status: 500, result: { 'error': err, 'message': 'unable to fetch players'}};
        });
    },

    // Parameters: { partieId, nbRounds }
    // Result: null
    // Function: update les rounds d'une partie
    updatePartieUtils: async function (partieId, nbRounds) {
        // Fetch the game by Id
        return models.Partie.findOne({
            attributes: ['id', 'nbRounds'],
            where: { id: partieId }
        }).then(function (partieFound) {
            // Verify game existence
            if (partieFound) {
                // Update game's number of rounds
                partieFound.update({
                    nbRounds: nbRounds
                }).catch(function (err) {
                    console.log(err);
                });
            } else {
                console.log("game not found");
            }
        }).catch(function (err) {
            console.log(err);
        });
    }
}