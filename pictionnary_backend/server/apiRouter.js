// Imports
var express = require('express');
var userCtrl = require('../routes/userCtrl');
var partieCtrl = require('../routes/partieCtrl');
var motCtrl = require('../routes/motCtrl');


// Router
exports.router = (function() {
    var apiRouter = express.Router();
    
    // Users routes
    apiRouter.route('/users/register/').post(userCtrl.register);
    apiRouter.route('/users/login/').post(userCtrl.login);
    //apiRouter.route('/users/logout/').post(userCtrl.logout);
    apiRouter.route('/users/:userId/').get(userCtrl.getUser);

    // Parties routes
    apiRouter.route('/parties/').post(partieCtrl.createPartie);
    apiRouter.route('/parties/').get(partieCtrl.listParties);
    apiRouter.route('/parties/:partieId/join/').post(partieCtrl.joinPartie);
    apiRouter.route('/parties/:partieId/leave/').post(partieCtrl.leavePartie);
    apiRouter.route('/parties/by/:userId').get(partieCtrl.listPartiesCrees);
    apiRouter.route('/parties/all/').get(partieCtrl.listPartiesAll);
    apiRouter.route('/parties/:partieId/').get(partieCtrl.getPartie);
    apiRouter.route('/parties/:partieId/').delete(partieCtrl.deletePartie);
    apiRouter.route('/parties/:partieId/empty/').post(partieCtrl.emptyPartie);

    // Words routes
    apiRouter.route('/words/').post(motCtrl.addWord);
    apiRouter.route('/words/').delete(motCtrl.deleteWord);
    apiRouter.route('/words/').get(motCtrl.listWords);


    return apiRouter;
})();