// Imports
var express = require('express');
var bodyParser = require('body-parser');
var apiRouter = require('./apiRouter').router;
const { getPartieUtils, updatePartieUtils } = require('../utils/partieUtils');
const { updateUserPointsUtils, updateWinnerUtils } = require('../utils/userUtils');


// Instantiate server
var app = express();
var server = require('http').createServer(app)
var io = require('socket.io')(server, {
    pingInterval: 10*60*1000,
    pingTimeout: 1000,
});

// Body Parser configuration
//app.use(cookieParser());

app.use(bodyParser.urlencoded({extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000");
    res.header("Access-Control-Allow-Credentials", "true");//accepter les cookies depuis le front
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization");
    res.header("Access-Control-Allow-Methods", "GET,POST,DELETE");
    
    next();
});


// Configure routes
app.get('/', function (req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.status(200).send('<h1>Bonjour sur mon super serveur</h1>');
});

app.use('/api/', apiRouter);



var rooms = [];

async function getRoom (roomId) {
    const myRoom = rooms.filter(room => room.roomId == roomId)[0];
    const myDbRoom = await getPartieUtils(roomId);
    if (myRoom){
        myRoom.places = myDbRoom.result.places;
        myRoom.players = myDbRoom.result.players;
        myRoom.nbRounds = myDbRoom.result.nbRounds;
        return myRoom;
    } else {
        const room = {
            roomId: roomId,
            drawHistory: [],
            partieDemarree: false,
            drawer: null,
            devineurs: [],
            secret: null,
            places: myDbRoom.result.places,
            players: myDbRoom.result.players,
            nbRounds: myDbRoom.result.nbRounds
        };
        rooms.push(room)
        return room;
    }
}
// Updates all players' number of games played
function updateWinner(room) {
    var bestScore = 0;
    var bestIndex = 0;
    for (var index in room.devineurs) {
        room.devineurs[index].best = false;
        if (room.devineurs[index].score > bestScore) {
            bestScore = room.devineurs[index].score;
            bestIndex = index;
        }
    }
    updateWinnerUtils(room.devineurs[bestIndex].userId);
}
function getNbrPrets(room) {
    return room.devineurs.map(item => item.pret).filter(item => item).length;
}
function allPrets(room) {
    return room.drawer.pret && getNbrPrets(room) === room.devineurs.length;
}
function setDevineurPret(room, socketId, pret) {
    for (var devineur of room.devineurs){
        if (devineur.socketId == socketId) {
            devineur.pret = pret;
        }
    }
}


io.on('connection', function(socket) {
    // A player wishes to join a room
    socket.on('join', async function (data) {
        console.log("join", data, socket.id)
        const room = await getRoom(data.roomId);
        // Make the player join the room
        socket.join(eval(room.roomId));
        // If the room has no drawer, this player is asigned as one
        if (room.drawer == null) {
            room.drawer = {
                socketId: socket.id,
                pret: false,
                socket: socket,
                userId: data.userId,
                username: data.username
            };
            // Inform the player of his drawing situation, and if the game has started (room.partieDemarree should be false)
            socket.emit('join', { 
                type: "drawer",
                partieDemarree: room.partieDemarree
            });
        } else {
            // If the room has a drawer
            console.log("a drawer is here, assigning to devineur")
            // Remember the player's socket as one of the devineurs
            room.devineurs.push({
                socketId: socket.id,
                pret: false,
                socket: socket,
                points: 0,
                userId: data.userId,
                username: data.username
            });
            // Inform the player of his guessing situation, and if the game has started
            socket.emit('join', { 
                type: "devineur",
                partieDemarree: room.partieDemarree
            });
            // If game has started, send all drawing until now
            if (room.partieDemarree) {
                for (var drawObj of room.drawHistory) {
                    socket.emit("draw", drawObj);
                }
            }
        }
        // Update user's localPoints to 0
        updateUserPointsUtils(data.userId, -1);
        
        // Display a connection message to the player
        socket.emit("message", { message: "you are connected", etat: "message", sender: "serveur" });
        // Display to the other players that a new player is now connected
        // The etat: "info" will enable players to refresh their room data
        socket.to(eval(data.roomId)).emit("message", { message: data.username + " connecté", etat: "info", sender: "serveur" });
        console.log(room.drawer.socketId, " with ", room.devineurs.map(item => item.socketId), " in room ", room.roomId)
    });
    // A player quits the game
    socket.on("deco", async function(data) {
        console.log("deco", data, socket.id)
        var room = await getRoom(data.roomId);
        if (room.drawer && room.drawer.socketId == socket.id) {
            // The drawer quits
            room.drawer = null;
            // Send a message etat: "drawer quit" to empty the room
            socket.to(eval(data.roomId)).emit("message", { message: "fin du jeu, drawer déconnecté", etat: "drawer quit", sender: "serveur" });

        } else {
            // Guesser Disconnects
            for (var i in room.devineurs) {
                if (room.devineurs[i].socketId == socket.id) {
                    room.devineurs.splice(i, 1);
                }
            }
            socket.to(eval(data.roomId)).emit("message", { message: data.username + " déconnecté", etat: "info", sender: "serveur" });
            
        }
        socket.disconnect();
    });
	socket.on('disconnect', function() {
        console.log("new deconnection socket:", socket.id)
    });
    // A player says wether he is ready or not
	socket.on('pret', async function(data) {
        console.log("pret", data, socket.id)
        var room = await getRoom(data.roomId);
        var drawer = room.drawer;
        // If the player is the drawer of the game
        if (drawer && socket.id == drawer.socketId) {
            drawer.pret = data.pret;
            if (drawer.pret) {
                // If the drawer is ready, ask all players in the room if they are ready
                socket.to(eval(room.roomId)).emit("pret", { pret: true });
                // Send a message to everybody to tell that drawer is ready
                socket.emit("message", { message: data.username + " started the game!", etat: "message", sender: "server" });
                socket.to(eval(data.roomId)).emit("message", { message: data.username + " started the game! Please tell if you are ready.", etat: "message", sender: "server" });
            } else {
                // If the drawer is not ready, tell everyone in the room to set to not ready
                socket.to(eval(room.roomId)).emit("pret", { pret: false });
                if (room.partieDemarree) {
                    // The game was in action, next round happening
                    // Update game's rounds left
                    updatePartieUtils(data.roomId, room.nbRounds-1);
                    // Send a message to everybody to tell to prepare for next round
                    socket.emit("message", { message: "Prepare for next round.", etat: "info", sender: "server" });
                    socket.to(eval(data.roomId)).emit("message", { message:"Prepare for next round.", etat: "info", sender: "server" });
                    // Set the game as stopped
                    room.partieDemarree = false;
                } else {
                    // The game was not started, it is an interruption from the drawer
                    socket.emit("message", { message: data.username + " interrupted the launch of the game.", etat: "message", sender: "server" });
                    socket.to(eval(data.roomId)).emit("message", { message: data.username + " interrupted the launch of the game.", etat: "message", sender: "server" });
                }
            
            }
        } else {
            // if the player is not drawer, set him ready
            setDevineurPret(room, socket.id, data.pret);
            // Send a message to everybody to tell that player is ready
            if (data.pret) {
                socket.emit("message", { message: data.username + " is ready.", etat: "message", sender: "server" });
                socket.to(eval(data.roomId)).emit("message", { message: data.username + " is ready.", etat: "message", sender: "server" });
            } else {
                socket.emit("message", { message: data.username + " isn't ready anymore.", etat: "message", sender: "server" });
                socket.to(eval(data.roomId)).emit("message", { message: data.username + " isn't ready anymore.", etat: "message", sender: "server" });
            }
        }
        // If everyone is ready
        if (allPrets(room)) {
            // Set room as started
            room.partieDemarree = true;
            // Reset all canvas
            socket.emit("clear");
            socket.to(eval(data.roomId)).emit("clear");
            room.drawHistory = [];
            // Tell everyone that the game is launched
            socket.emit("partie lancee");
            socket.to(eval(room.roomId)).emit("partie lancee");
            // Send a message to everyone that the game is launched
            socket.emit("message", { message: "Game is started!", etat: "message", sender: "server" });
            socket.to(eval(data.roomId)).emit("message", { message: "Game is started!", etat: "message", sender: "server" });
        }
    });
    // Reception of a line draw and reemission to everyone other than the drawer
	socket.on('draw', async function(data) {
        console.log(data)
        var room = await getRoom(data.roomId);
        const drawObj = { lines: data.lines, width: data.width, color: data.color };
        room.drawHistory.push(drawObj);
		socket.to(eval(room.roomId)).emit("draw", drawObj);
    });
    // Reception of a clear message and reemission to everyone
	socket.on('clear', async function(data) {
        var room = await getRoom(data.roomId);
        room.drawHistory = [];
        socket.emit("clear");
		socket.to(eval(room.roomId)).emit("clear");
    });
    // Remove last line
	socket.on('clear last', async function(data) {
        var room = await getRoom(data.roomId);
        room.drawHistory.splice(-1,1);
        socket.emit("clear");
        socket.to(eval(room.roomId)).emit("clear");
        for (const drawObj of room.drawHistory) {
            socket.emit("draw", drawObj);
            socket.to(eval(room.roomId)).emit("draw", drawObj);
        }
    });
    // Reception of the secret word from the drawer, saving it
    socket.on('secret', async function(data) {
        console.log("setting secret", data)
        var room = await getRoom(data.roomId);
        room.secret = data.word;
    });
    // Reception of a message
    // Retransmission to all players [ "message", "info"]
	socket.on('message', async function(data) {
        console.log("message", data)
        var room = await getRoom(data.roomId);
        // Resend in broadcast
        socket.emit("message", { message: data.message, etat: "message", sender: data.sender });
        socket.to(eval(data.roomId)).emit("message", { message: data.message, etat: "message", sender: data.sender });

        // If the game is on and the message is the secret
        if (data.message == room.secret && room.partieDemarree) {
            // Update user's nbPoints and nbLocalPoints
            updateUserPointsUtils(data.userId, data.nbPoints);

            // Send a winning message
            // etat: "info" so that players refresh the room information
            socket.emit("message", { message: "Congrats " + data.sender + "!!! He won " + data.nbPoints + "pts!", etat: "info", sender: "Server" });
            socket.to(eval(data.roomId)).emit("message", { message: "Congrats " + data.sender + "!!! He won " + data.nbPoints + "pts!", etat: "info", sender: "Server" });
            socket.emit("alert", { message: data.sender + " is the winner!" });
            socket.to(eval(data.roomId)).emit("alert", { message: data.sender + " is the winner!" });

            if (room.nbRounds > 1) {
                // More rounds incomming
                // Set the drawer to not ready, it will launch the next round
                room.drawer.socket.emit("pret", { pret: false });
            } else {
                // Update winner's number of wins
                updateWinner(room);

                // No more rounds, the game is ended
                room.partieDemarree = false;
                socket.emit("message", { message: "No more rounds, game is ended, thank you for playing!", etat: "info", sender: "Server" });
                socket.to(eval(data.roomId)).emit("message", { message: "No more rounds, game is ended, thank you for playing!", etat: "info", sender: "Server" });    
            }
        }
	});
});



// Launch server
const port = 8080;
server.listen(port, function() {
    console.log("Server a l'ecoute sur le port ", port);
});
